#include <FastLED.h>
#include <SPI.h>

#define RADIUS 6
#define NUM_LEDS 3 * RADIUS * (RADIUS - 1) + 1
#define DATA_PIN 10
#define LATCH 11

int hexcounterTot(int line);  //number of points that precede 'line' linearly
int hexcounterNum(int line);  //number of points in any given line
void initConverters();

bool board[RADIUS*2 - 1][RADIUS*2 - 1];
bool pBoard[RADIUS*2 - 1][RADIUS*2 - 1];
int mode = 0;
int editSect = 0;
int rotation = 1;
int buttSector[6][90] = {0};
int converter[3][91];
//int sectChannel[6][6] = {0};
byte shiftBank;

int keyShift[6][6] = {{33, 33, 33, 33, 33, 33}};
int colShift[6][6] = {{-3, -3, -3, -3, -3, -3}};
int layout[6][6] = {{0},{1},{2},{3},{4},{5}};
int gKeyShift[6] = {0,0,0,0,0,0};
int gColShift[6] = {0,0,0,0,0,0};
int sentMIDI[6][6][128];
int recMIDI[16][128];

CRGB leds[NUM_LEDS];

int modeColor[6] = {
  0xFF0000,
  0x888800,
  0x00FF00,
  0x008888,
  0x0000FF,
  0x880088
};

int harmColor[6][6][2] = {
{{0x227722, 0x555555},{0x227722, 0x555555},{0x227722, 0x555555},{0x227722, 0x555555},{0x227722, 0x555555},{0x227722, 0x555555}}
};

void OnNoteOn(byte channel, byte note, byte velocity)
{
  //recMIDI[channel][note] = velocity;
  recMIDI[1][note] = velocity;
}

void OnNoteOff(byte channel, byte note, byte velocity)
{
  //recMIDI[channel][note] = velocity;
  recMIDI[1][note] = 0;
}

void setup() {
  SPI.begin ();
  usbMIDI.setHandleNoteOff(OnNoteOff);
  usbMIDI.setHandleNoteOn(OnNoteOn) ;
  FastLED.addLeds<WS2812B, DATA_PIN, GRB>(leds, NUM_LEDS);
  pinMode (LATCH, OUTPUT);
  digitalWrite (LATCH, HIGH);
  initConverters();
  //usbMIDI.sendControlChange()
  clearMIDI();
}

void loop() {
  shiftRead();
  //button registry--------------------------------------------
  if (board[posI(0,0)][posJ(0,0)]) { //i and j dont need to be rotated because its the middle
    if (rEdge(4,4)){clearMIDI(); gKeyShift[mode]++;}
    if (rEdge(6,6)){clearMIDI(); gKeyShift[mode]--;}
    if (rEdge(5,4)){gColShift[mode]++;}
    if (rEdge(4,5)){gColShift[mode]--;}
    if (rEdge(5,6)){clearMIDI(); gKeyShift[mode]+=12;}
    if (rEdge(5,5)){clearMIDI(); gKeyShift[mode] = 0; gColShift[mode] = 0;}
    if (rEdge(6,5)){clearMIDI(); gKeyShift[mode]-=12;}
    for (int eachSector = 0; eachSector < 6; eachSector++) {
      if (keyShift[mode][eachSector] < 0) {keyShift[mode][eachSector] = 0;}
      else if (keyShift[mode][eachSector] > 60) {keyShift[mode][eachSector] = 60;}
    }
  }
  for (int i = 0; i < RADIUS*2 - 1; i++) {
    for (int j = 0; j < RADIUS*2 - 1; j++) {
      if (abs(i-j) < RADIUS) {
        //exceptions
        if (board[posI(0,0)][posJ(0,0)] && (i==10 && j==10) && rEdge(10,10)) {
          editMode();
        }
        else if (board[posI(0,0)][posJ(0,0)] && (i+j == 5) && rEdge(i,j)){
          clearMIDI();
          mode = j;
        }
        else if (board[posI(0,0)][posJ(0,0)] && (abs(i-j) < 2 && abs(i+j-10) < 3)) {} //i and j dont need to be rotated because its the middle
        else if (i == 0 && j == 0) {}
        //midi
        else if (edge(i,j) && buttSector[mode][hexcounterTot(i) + j + ((i < 7)?0:(i - 6))] != 6) {
          int keyPH = 4*(10-i) + 3*(10-j) - 3 + keyShift[mode][sectCon(i,j)] + gKeyShift[mode];
          if (board[posI(i,j)][posJ(i,j)]) {
            if(!sentMIDI[mode][sectCon(i,j)][keyPH]){
              usbMIDI.sendNoteOn(keyPH, 127, sectCon(i,j) + 1);
            }
            sentMIDI[mode][buttSector[mode][hexcounterTot(posI(i,j)) + posJ(i,j)]][keyPH]++;
          }
          else {
            if(sentMIDI[mode][buttSector[mode][hexcounterTot(posI(i,j)) + posJ(i,j)]][keyPH]){
              usbMIDI.sendNoteOn(keyPH, 0, sectCon(i,j) + 1);
              sentMIDI[mode][buttSector[mode][hexcounterTot(posI(i,j)) + posJ(i,j)]][keyPH]--;
            } 
          }
        }
        pBoard[posI(i,j)][posJ(i,j)] = board[posI(i,j)][posJ(i,j)];
      }
    }
  }
  //lights----------------------------------------------------
  for (int i = 0; i < RADIUS*2 - 1; i++) {
    for (int j = 0; j < RADIUS*2 - 1; j++) {
      if (abs(i-j) < RADIUS) {
        int keyPH = 4*(10-i) + 3*(10-j) - 3 + keyShift[mode][sectCon(i,j)] + gKeyShift[mode];
        usbMIDI.read();
        //if (recMIDI[sectChannel[mode][buttSector[mode][sectCon(i,j)][keyPH]){
        if (recMIDI[sectCon(i,j) + 1][keyPH]){
          leds[ledCon(i,j)] = modeColor[mode];
        }
        else if (buttSector[mode][hexcounterTot(i) + j + ((i < 7)?0:(i - 6))] == 6) {
          leds[ledCon(i,j)] = CRGB::Black;
        }
        else if (board[posI(i,j)][posJ(i,j)]) {
          leds[ledCon(i,j)] = harmColor[mode][sectCon(i,j)][bwCheck(4*(10-i) + 3*(10-j) + keyShift[mode][sectCon(i,j)] + colShift[mode][sectCon(i,j)] + gKeyShift[mode] + gColShift[mode])];
        }
        else {
          leds[ledCon(i,j)] = (harmColor[mode][sectCon(i,j)][bwCheck(4*(10-i) + 3*(10-j) + keyShift[mode][sectCon(i,j)] + colShift[mode][sectCon(i,j)] + gKeyShift[mode] + gColShift[mode])] & ~0x030303)/ 4;
        }
        leds[ledCon(0,0)] = (modeColor[mode] & ~0x030303)/ 4;
        if (board[posI(0,0)][posJ(0,0)]) {
          if (i+j == 5){
            leds[ledCon(i,j)] = (modeColor[j] & ~0x010101)/ 2;
          }
          leds[ledCon(6,6)] = board[posI(6,6)][posJ(6,6)]?modeColor[mode]:((modeColor[mode] & ~0x030303)/ 4);
          leds[ledCon(6,5)] = board[posI(6,5)][posJ(6,5)]?modeColor[mode]:((modeColor[mode] & ~0x030303)/ 4);
          leds[ledCon(5,4)] = board[posI(5,4)][posJ(5,4)]?modeColor[mode]:((modeColor[mode] & ~0x030303)/ 4);
          leds[ledCon(5,6)] = board[posI(5,6)][posJ(5,6)]?modeColor[mode]:((modeColor[mode] & ~0x030303)/ 4);
          leds[ledCon(4,5)] = board[posI(4,5)][posJ(4,5)]?modeColor[mode]:((modeColor[mode] & ~0x030303)/ 4);
          leds[ledCon(4,4)] = board[posI(4,4)][posJ(4,4)]?modeColor[mode]:((modeColor[mode] & ~0x030303)/ 4);
          leds[ledCon(5,5)] = board[posI(5,5)][posJ(5,5)]?modeColor[mode]:((~modeColor[mode]) & ~0x070707)/ 8;
          leds[ledCon(10,10)]=board[posI(10,10)][posJ(10,10)]?modeColor[mode]:((modeColor[mode] & ~0x030303)/ 4);
          leds[ledCon(0,0)] = modeColor[mode];
        }
      }
    }
  }
  FastLED.show();
}

void editMode() {
  clearMIDI();
  clearInCache();
  int inEditMode = 1;
  for (int i = 0; i < 91; i++) {
    leds[i] = CRGB::Black;
  }
  FastLED.show();
  pBoard[posI(10,10)][posJ(10,10)] = 1;
  while (inEditMode){
    shiftRead();
    if (board[posI(0,0)][posJ(0,0)]) { //i and j dont need to be rotated because its the middle
      if (rEdge(4,4)){clearMIDI(); keyShift[mode][editSect]++;}
      if (rEdge(6,6)){clearMIDI(); keyShift[mode][editSect]--;}
      if (rEdge(5,4)){colShift[mode][editSect]++;}
      if (rEdge(4,5)){colShift[mode][editSect]--;}
      if (rEdge(5,6)){clearMIDI(); keyShift[mode][editSect]+=12;}
      if (rEdge(5,5)){clearMIDI(); smartColor();}
      if (rEdge(6,5)){clearMIDI(); keyShift[mode][editSect]-=12;}
      if (rEdge(5,10)){rotation = (rotation + 5) % 6;} //same as -1 except allows for 0 - 1 to equal 5
      if (rEdge(10,5)){rotation = (rotation + 1) % 6;}
      if (rEdge(10,10)) {inEditMode = 0;}
      if (keyShift[mode][editSect] < 0) {keyShift[mode][editSect] = 0;}
      else if (keyShift[mode][editSect] > 60) {keyShift[mode][editSect] = 60;}
    }
    for (int i = 0; i < RADIUS*2 - 1; i++) {
      for (int j = 0; j < RADIUS*2 - 1; j++) {
        if (abs(i-j) < RADIUS) {
          if (i == 0 && j == 0){}
          else if (board[posI(0,0)][posJ(0,0)]){
            if ((i+j == 5) && rEdge(i,j)){
              editSect = j;
            }
            else if ((abs(i-j) < 2 && abs(i+j-10) < 3)) { //i and j dont need to be rotated because its the middle
            }
            else if (rEdge(10,10)){}
            else if (rEdge(i,j)) {
              editSect = 6;
            }
          }
          else if (edge(i,j)) {
            if (buttSector[mode][hexcounterTot(i) + j + ((i < 7)?0:(i - 6))] == editSect) {
              int keyPH = 4*(10-i) + 3*(10-j) - 3 + keyShift[mode][sectCon(i,j)] + gKeyShift[mode];
              if (board[posI(i,j)][posJ(i,j)]) {
                if(!sentMIDI[mode][sectCon(i,j)][keyPH]){
                  usbMIDI.sendNoteOn(keyPH, 127, sectCon(i,j) + 1);
                }
                sentMIDI[mode][buttSector[mode][hexcounterTot(posI(i,j)) + posJ(i,j)]][keyPH]++;
              }
              else {
                  usbMIDI.sendNoteOn(keyPH, 0, sectCon(i,j) + 1);
                if(sentMIDI[mode][buttSector[mode][hexcounterTot(i) + j + ((i < 7)?0:(i - 6))]][keyPH]){
                  sentMIDI[mode][buttSector[mode][hexcounterTot(i) + j + ((i < 7)?0:(i - 6))]][keyPH]--;
                } 
              }
            }
            else {
              buttSector[mode][hexcounterTot(i) + j + ((i < 7)?0:(i - 6))] = editSect;
            }
          }
          pBoard[posI(i,j)][posJ(i,j)] = board[posI(i,j)][posJ(i,j)];
        }
      }
    }
    //edit lights----------------------------------------------------
    for (int i = 0; i < RADIUS*2 - 1; i++) {
      for (int j = 0; j < RADIUS*2 - 1; j++) {
        if (abs(i-j) < RADIUS) {
          if (buttSector[mode][hexcounterTot(i) + j + ((i < 7)?0:(i - 6))] == 6) {
            leds[ledCon(i,j)] = CRGB::Black;
          }
          else {
              //int keyPH = 4*(10-i) + 3*(10-j) - 3 + keyShift[mode][buttSector[mode][hexcounterTot(posI(i,j)) + posJ(i,j)]] + gKeyShift[mode];
              leds[ledCon(i,j)] = blend(0x000000, blend(modeColor[sectCon(i,j)], modeColor[mode], 127), 30 + 40 * bwCheck(4*(10-i) + 3*(10-j) + keyShift[mode][sectCon(i,j)] + colShift[mode][sectCon(i,j)] + gKeyShift[mode] + gColShift[mode]));
              leds[ledCon(0,0)] = (modeColor[mode] & ~0x030303)/ 4;
          }
          if (board[posI(0,0)][posJ(0,0)]) {
            if (i+j == 5){
              leds[ledCon(i,j)] = blend(0x000000, blend(modeColor[j], modeColor[mode], 127), 75);
            }
          }
        }
      }
    }
    if (board[posI(0,0)][posJ(0,0)]) {
      leds[ledCon(6,6)] = board[posI(6,6)][posJ(6,6)]?modeColor[mode]:(((modeColor[mode] & ~0x030303)/ 4) + 0x111111);
      leds[ledCon(6,5)] = board[posI(6,5)][posJ(6,5)]?modeColor[mode]:(((modeColor[mode] & ~0x030303)/ 4) + 0x111111);
      leds[ledCon(5,4)] = board[posI(5,4)][posJ(5,4)]?modeColor[mode]:(((modeColor[mode] & ~0x030303)/ 4) + 0x111111);
      leds[ledCon(5,6)] = board[posI(5,6)][posJ(5,6)]?modeColor[mode]:(((modeColor[mode] & ~0x030303)/ 4) + 0x111111);
      leds[ledCon(4,5)] = board[posI(4,5)][posJ(4,5)]?modeColor[mode]:(((modeColor[mode] & ~0x030303)/ 4) + 0x111111);
      leds[ledCon(4,4)] = board[posI(4,4)][posJ(4,4)]?modeColor[mode]:(((modeColor[mode] & ~0x030303)/ 4) + 0x111111);
      leds[ledCon(5,5)] = board[posI(5,5)][posJ(5,5)]?modeColor[mode]:((~modeColor[mode]) & ~0x070707)/ 8;
      leds[ledCon(5,10)] = 0x505050;
      leds[ledCon(10,5)] = 0x505050;
      leds[ledCon(10,10)]=board[posI(10,10)][posJ(10,10)]?modeColor[mode]:((modeColor[mode] & ~0x030303)/ 4);
      leds[ledCon(0,0)] = blend(0x000000, blend(modeColor[editSect], modeColor[mode], 127), 200);
    }
    else {  
      leds[ledCon(0,0)] = blend(0x000000, blend(modeColor[editSect], modeColor[mode], 127), 75);
    }
    if (editSect == 6) {
      leds[ledCon(0,0)] = CRGB::Black;
    }
    FastLED.show();
  }
  clearMIDI();
}

void smartColor() {
  bool smartColArray[12] = {0};
  int inSmartColor = 1;
  while (board[5][5]) {shiftRead();} //can leave as 5/5 because it doesnt change based on rotation
  while (inSmartColor) {
    shiftRead();
    for (int i = 0; i < RADIUS*2 - 1; i++) {
      for (int j = 0; j < RADIUS*2 - 1; j++) {
        if (abs(i-j) < RADIUS) {
          if (board[i][j] != pBoard[i][j]) {
            int keyPH = 4*(10-i) + 3*(10-j) - 3 + keyShift[mode][buttSector[mode][hexcounterTot(i) + j]] + gKeyShift[mode];
            if (board[i][j]) {
              if(!sentMIDI[mode][buttSector[mode][hexcounterTot(i) + j]][keyPH]){
                usbMIDI.sendNoteOn(keyPH, 127, 1);
              }
              sentMIDI[mode][buttSector[mode][hexcounterTot(i) + j]][keyPH] += 1;
              smartColArray[keyPH%12] = 1;
            }
            else {
              if(sentMIDI[keyPH]){
                usbMIDI.sendNoteOn(keyPH, 0, 1);
              }
              sentMIDI[mode][buttSector[mode][hexcounterTot(i) + j]][keyPH] -= 1;
            }
          }
          pBoard[i][j] = board[i][j];
        }
      }
    }
    for (int i = 0; i < RADIUS*2 - 1; i++) {
      for (int j = 0; j < RADIUS*2 - 1; j++) {
        if (abs(i-j) < RADIUS) {
          if (smartColArray[(4*(10-i) + 3*(10-j) - 3 + keyShift[mode][buttSector[mode][hexcounterTot(i) + j]])%12]) {
            leds[hexcounterTot(i+i%2) + ((i%2)?-j-1:j) - ( (i <= 5)?0:(((i+1)%2)?(i - 5) :(-i + 5)))] = harmColor[mode][buttSector[mode][hexcounterTot(i) + j + ((i < 7)?0:(i - 6))]][1];
          }
          else {
            leds[hexcounterTot(i+i%2) + ((i%2)?-j-1:j) - ( (i <= 5)?0:(((i+1)%2)?(i - 5) :(-i + 5)))] = (harmColor[mode][buttSector[mode][hexcounterTot(i) + j + ((i < 7)?0:(i - 6))]][1] & ~0x030303)/ 4;
          }
        }
      }
    }
    leds[45].r = 127*(sin(millis()/50) + 1);
    FastLED.show();
    //check for key perfection
    int numFit = 0;
    int candidate = 0;
    for (int i = 0; i < 12; i++) {
      bool fit = 1;
      for (int j = 0; j < 12; j++) {
        if (smartColArray[j] && !bwCheck((j + i)%12)) {
          fit = 0;
        }
      }
      if (fit) {numFit++; candidate = i;}
    }
    if (numFit == 1) {
      colShift[mode][editSect] = candidate - 3;
      return;
    }
    else if (!numFit) {
      return;
    }
  }
}

void initConverters() {
  int index = 0;
  for (int i = 0; i < RADIUS*2 - 1; i++) {
    for (int j = 0; j < RADIUS*2 - 1; j++) {
      if (abs(i-j) < RADIUS) {
        converter[0][index] = 4*(10-i) + 3*(10-j) - 3;
        index++;
      }
    }
  }
  index = 0;
  for (int i = 0; i < RADIUS*2 - 1; i++) {
    for (int j = 0; j < RADIUS*2 - 1; j++) {
      if (abs(i-j) < RADIUS) {
        converter[1][index] = 4*(10-i) + 3*(10-j) - 3;
        index++;
      }
    }
  }
}

void clearMIDI() {
  for (int i = 0; i < RADIUS*2 - 1; i++) {
    for (int j = 0; j < RADIUS*2 - 1; j++) {
      if (abs(i-j) < RADIUS) {
        int keyPH = 4*(10-i) + 3*(10-j) - 3 + keyShift[mode][buttSector[mode][hexcounterTot(i) + j]] + gKeyShift[mode];
        sentMIDI[mode][buttSector[mode][hexcounterTot(i) + j]][keyPH] = 0;
        usbMIDI.sendNoteOn(keyPH, 0, 1);
      }
    }
  }
}

void clearInCache() {
  for (int i = 0; i < 6; i++) {
    for (int j = 0; j < 127; j++) {
      recMIDI[i][j] = 0;
    }
  }
}

void shiftRead() {
  int rowInd = 0;
  digitalWrite (LATCH, LOW);
  digitalWrite (LATCH, HIGH);
  for (int i = 0; i < 3 * RADIUS * (RADIUS - 1) + 1; i++) {
    if (!(i%8)) shiftBank = SPI.transfer(0);
    rowInd += (hexcounterTot(rowInd) == i); //increment rowInd if the index is the beginning of the next row
    board[rowInd - 1][i - hexcounterTot(rowInd - 1) + ((rowInd < 7)?0:(rowInd - 6))] = shiftBank & (1<<(i%8));
  }
}

int posI(int i, int j) {
  switch(rotation) {
    case 1:
      return j;
    case 2:
      return 5 - i + j;
    case 3:
      return 10 - i;
    case 4:
      return 10 - j;
    case 5:
      return i - j + 5;
    default:
      return i;
  }
}

int posJ(int i, int j) {
  switch(rotation) {
    case 1:
      return 5 - i + j;
    case 2:
      return 10 - i;
    case 3:
      return 10 - j;
    case 4:
      return i - j + 5;
    case 5:
      return i;
    default:
      return j;
  }
}

bool bwCheck(int key) {return (key)%12 == 0 || (key)%12 == 2 || (key)%12 == 4 || (key)%12 == 5 || (key)%12 == 7 || (key)%12 == 9 || (key)%12 == 11;} //1 if white 0 if black

int ledCon(int i, int j) {return hexcounterTot(posI(i,j)+posI(i,j)%2) + ((posI(i,j)%2)?-posJ(i,j)-1:posJ(i,j)) - ( (posI(i,j) <= 5)?0:(((posI(i,j)+1)%2)?(posI(i,j) - 5) :(-posI(i,j) + 5)));}
int sectCon(int i, int j) {return buttSector[mode][hexcounterTot(i) + j + ((i < 7)?0:(i - 6))];}

int hexcounterTot(int line) {return (line*(2 * RADIUS - 1) - (RADIUS*(RADIUS - 1) + (line - RADIUS + 1)*abs(line - RADIUS)) / 2);}
int hexcounterNum(int line) {return ((2 * RADIUS - 1) - abs(line - RADIUS));}

bool edge(int i, int j) {return board[posI(i,j)][posJ(i,j)] != pBoard[posI(i,j)][posJ(i,j)];}
bool rEdge(int i, int j) {return board[posI(i,j)][posJ(i,j)] && !pBoard[posI(i,j)][posJ(i,j)];}

