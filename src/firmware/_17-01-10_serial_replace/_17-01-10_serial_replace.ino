#include <FastLED.h>
#include <SPI.h>

#define RADIUS 6
#define TOTAL 3 * RADIUS * (RADIUS - 1) + 1
#define DATA_PIN 10
#define LATCH 11
#define MAX_NOTE_IN 16

bool buffA[TOTAL];
bool buffB[TOTAL];
bool* board;
bool* pBoard;
int mode = 0;
int editSect = 0;
int rotation = 0;
int buttSector[6][TOTAL] = {0};
int converter[3][TOTAL];
int rowArr[91];
int ledArr[91];
byte shiftBank;

int keyShift[6][6] = {{33, 33, 33, 33, 33, 33},{33, 33, 33, 33, 33, 33},{33, 33, 33, 33, 33, 33}};
int colShift[6][6];
int layout[6][6] = {{0},{1},{0},{0},{0},{0}};
//new array with: 0:harmonic, 1:wickihayden, 2:chordmacro, 3: drums, 4: user
int gKeyShift = 0;
int gColShift = 0;
int recMIDI[16][MAX_NOTE_IN];
int recI = 0;
bool sustain[6][6] = {{},{},{1,1,1,1,1,1}}; //for chord macro: bass,chord,inversion,sus+add

int cmType[TOTAL] = {
           0, 14, 13,  9,  4,  3,
        12, 16, 17, 10,  8,  2,  1,
      11, 15,  0,  0,  0,  0,  0,  0,
     6,  7,  0,  0,  0,  0,  0,  0,  0,
   4,  5,  0,  0,  0,  0,  0,  0,  0,  0,
 3,  2,  0,  0,  0,  0,  0,  0,  0,  0,  0,
   1,  0,  0,  0,  0,  0,  0,  0,  0,  0,
     0,  0,  0,  0,  0,  0,  0,  0,  0,
       0,  0,  0,  0,  0,  0,  0,  0,
         0,  0,  0,  0,  0,  0,  0,
           0,  0,  0,  0,  0,  0
};

int drums[TOTAL] = {
           0,  0,  0,  0,102,101,
         0, 75, 74,  0,100, 99, 98,
       0, 73, 72, 71,  0, 97, 96,  0,
     0,  0, 70, 69, 68,  0,  0,  0,  0,
  90, 89,  0, 67, 66, 65,  0,  0, 54, 53,
88, 87, 86,  0, 64, 63, 62,  0, 52, 51, 50,
  85, 84,  0,  0, 61, 60,  0,  0, 49, 48,
     0,  0,  0,  0,  0,  0,  0,  0,  0,
       0, 42, 41,  0,  0, 30, 29,  0,
        40, 39, 38,  0, 28, 27, 26,
          37, 36,  0,  0, 25, 24
};
int cmDic[][4] = {
  {1,5,8,0},
  {1,5,8,12},
  {1,5,8,11},
  {1,4,8,0},
  {1,4,8,11},
  {1,4,7,11},
  {1,5,9,0},
  {1,5,10,0},
  {1,4,7,0},
  {1,4,7,10}
};
int cmBass = 60;
int cmBassP = 0;
int cmChord = 0;
int cmInv = 0;
int cmInvP = 0;
bool cmMods[3];
bool cmModsP[3];
bool cmChordOn = 0;

CRGB leds[TOTAL];

int modeColor[7] = {
  0xFF0000,
  0x808000,
  0x00FF00,
  0x008080,
  0x0000FF,
  0x800080,
  0x555555
};

int harmColor[6][6][2] = {
{{0x227722, 0x555555},{0x227722, 0x555555},{0x227722, 0x555555},{0x227722, 0x555555},{0x227722, 0x555555},{0x227722, 0x555555}},
{{0x227722, 0x555555},{0x227722, 0x555555},{0x227722, 0x555555},{0x227722, 0x555555},{0x227722, 0x555555},{0x227722, 0x555555}},
{{0x227722, 0x555555},{0x227722, 0x555555},{0x227722, 0x555555},{0x227722, 0x555555},{0x227722, 0x555555},{0x227722, 0x555555}}
};

void setup() {
  Serial.begin(9600);
  SPI.begin ();
  FastLED.addLeds<WS2812B, DATA_PIN, GRB>(leds, TOTAL);
  FastLED.setBrightness(255);
  pinMode (LATCH, OUTPUT);
  digitalWrite (LATCH, HIGH);
  initConverters();
  board = &buffA[0];
  pBoard = &buffB[0];
  //usbMIDI.sendControlChange()
  clearMIDI();
}
bool feahb = 0;
void loop() {
  /*if (feahb) {
    board = &buffA;
    pBoard = &buffB;
  }
  else {
    board = &buffB;
    pBoard = &buffA;
  }
  feahb = !feahb;*/
  bool* temp = pBoard;
  pBoard = board;
  board = temp;
  digitalWrite (LATCH, LOW);
  digitalWrite (LATCH, HIGH);
  for (int i = 0; i < TOTAL; i++) {
    if (!(i%8)) {shiftBank = SPI.transfer(0);}
    board[i] = shiftBank & (1<<(i%8));
  }
  //button registry--------------------------------------------
  if (board[roti(0)]) {
    if (rEdge(34)){clearMIDI(); gKeyShift++;}
    if (rEdge(56)){clearMIDI(); gKeyShift--;}
    if (rEdge(35)){gColShift++;}
    if (rEdge(44)){gColShift--;}
    if (rEdge(46)){clearMIDI(); gKeyShift+=12;}
    if (rEdge(45)){clearMIDI(); gKeyShift = 0; gColShift = 0;}
    if (rEdge(55)){clearMIDI(); gKeyShift-=12;}
    for (int eachSector = 0; eachSector < 6; eachSector++) {
      if (keyShift[mode][eachSector] < 0) {keyShift[mode][eachSector] = 0;}
      else if (keyShift[mode][eachSector] > 60) {keyShift[mode][eachSector] = 60;}
    }
  }
  Serial.println((int)board);
  for (int i = 0; i < TOTAL; i++) {
    Serial.print(board[i]);
    //Serial.print(",");
    //Serial.println(pBoard[i]);
    //exceptions
    /*if (board[roti(0)] && (i==90) && rEdge(90)) {
      editMode();
    }*/ //no more edit mode
    if (rEdge(0)) {
      clearInCache();
    }
    else if (board[roti(0)] && ((rowArr[i] - 1 + i - hexcounterTot(rowArr[i] - 1) + ((rowArr[i] < 7)?0:(rowArr[i] - 6))) == 5) && rEdge(i)){ //horizontal row
      clearMIDI();
      mode = 6 - rowArr[i];
    }
    else if (board[roti(0)] && (i == 34 || i == 35 || i == 44 || i == 45 || i == 46 || i == 55 || i == 56)) {} //i and j dont need to be rotated because its the middle
    else if (i == 0) {}
    
    //midi
    else if (edge(i) && !buttSector[mode][i]) {
      if (rEdge(i)) {
        /*Serial.print(i);
        Serial.println(" on");*/
      }
      else {
        /*Serial.print(i);
        Serial.println(" off");*/
      }
    }
    delay(10);
  }
  Serial.println("b");
  Serial.println((int)pBoard);
  for (int i = 0; i < TOTAL; i++) {
    Serial.print(pBoard[i]);
    delay(10);
  }
  Serial.println("s");


  //lights----------------------------------------------------


  for (int i = 0; i < TOTAL; i++) {
    int keyPH = converter[layout[mode][buttSector[mode][i]]][i] + keyShift[mode][buttSector[mode][i]] + gKeyShift;
    if (buttSector[mode][i]) {
      leds[ledArr[roti(i)]] = CRGB::Black;
    }
    else if (board[roti(i)]) {
      leds[ledArr[roti(i)]] = harmColor[mode][buttSector[mode][i]][bwCheck(converter[layout[mode][buttSector[mode][i]]][i] + keyShift[mode][buttSector[mode][i]] + colShift[mode][buttSector[mode][i]] + gKeyShift + gColShift)];
    }
    else {
      leds[ledArr[roti(i)]] = (harmColor[mode][buttSector[mode][i]][bwCheck(converter[layout[mode][buttSector[mode][i]]][i] + keyShift[mode][buttSector[mode][i]] + colShift[mode][buttSector[mode][i]] + gKeyShift + gColShift)] & ~0x030303)/ 4;
    }
    if (board[roti(0)]) {
      if ((rowArr[i] - 1 + i - hexcounterTot(rowArr[i] - 1) + ((rowArr[i] < 7)?0:(rowArr[i] - 6))) == 5){
        leds[ledArr[roti(i)]] = (modeColor[6 - rowArr[i]] & ~0x010101)/ 2;
      }
      leds[ledArr[56]] = board[56]?modeColor[mode]:((modeColor[mode] & ~0x030303)/ 4);
      leds[ledArr[46]] = board[46]?modeColor[mode]:((modeColor[mode] & ~0x030303)/ 4);
      leds[ledArr[35]] = board[35]?modeColor[mode]:((modeColor[mode] & ~0x030303)/ 4);
      leds[ledArr[55]] = board[55]?modeColor[mode]:((modeColor[mode] & ~0x030303)/ 4);
      leds[ledArr[44]] = board[44]?modeColor[mode]:((modeColor[mode] & ~0x030303)/ 4);
      leds[ledArr[34]] = board[34]?modeColor[mode]:((modeColor[mode] & ~0x030303)/ 4);
      leds[ledArr[45]] = board[45]?modeColor[mode]:((~modeColor[mode]) & ~0x070707)/ 8;
      leds[ledArr[roti(0)]] = modeColor[mode];
      //leds[ledArr[roti(90)]] = board[90]?modeColor[mode]:((modeColor[mode] & ~0x030303)/ 4);
    }
    else {
      leds[ledArr[roti(0)]] = (modeColor[mode] & ~0x030303)/ 4;
    }
  }
  FastLED.show();
}

void initConverters() {
  int index = 0;
  for (int i = 0; i < RADIUS*2 - 1; i++) {
    for (int j = 0; j < RADIUS*2 - 1; j++) {
      if (abs(i-j) < RADIUS) {
        converter[0][index] = 4*(10-i) + 3*(10-j) - 3; //harmonic
        index++;
      }
    }
  }
  index = 0;
  for (int i = 0; i < RADIUS*2 - 1; i++) {
    for (int j = 0; j < RADIUS*2 - 1; j++) {
      if (abs(i-j) < RADIUS) {
        converter[1][index] = 5*(10-i) - 7*(10-j) + 45; //wicki hayden
        index++;
      }
    }
  }
  index = 0;
  for (int i = 0; i < RADIUS*2 - 1; i++) {
    for (int j = 0; j < RADIUS*2 - 1; j++) {
      if (abs(i-j) < RADIUS) {
        ledArr[index] = hexcounterTot(i + i%2) + ((i%2)?-j-1:j) - ((i <= 5)?0:(((i+1)%2)?(i - 5):(-i + 5))); //led matrix
        index++;
      }
    }
  }
  index = 0;
  for (int rows = 1; rows < (2 * RADIUS); rows++) {
    for (int units = 0; units < hexcounterNum(rows); units++) {
      rowArr[index] = rows;                                                       //converts indices into row numbers
      index++;
    }
  }
}

void clearMIDI() {
  for (int i = 0; i < RADIUS*2 - 1; i++) {
    for (int j = 0; j < RADIUS*2 - 1; j++) {
      if (abs(i-j) < RADIUS) {
      }
    }
  }
}

void clearInCache() {
  for (int i = 0; i < 6; i++) {
    for (int j = 0; j < MAX_NOTE_IN; j++) {
      recMIDI[i][j] = 0;
    }
  }
}

void shiftRead() {
  //int rowInd = 0;
  digitalWrite (LATCH, LOW);
  digitalWrite (LATCH, HIGH);
  for (int i = 0; i < TOTAL; i++) {
    if (!(i%8)) {shiftBank = SPI.transfer(0);}
    board[i] = shiftBank & (1<<(i%8));
  }
}

int posI(int i, int j) {
  switch(rotation) {
    case 1:
      return j;
    case 2:
      return (RADIUS-1) - i + j;
    case 3:
      return 2*(RADIUS-1) - i;
    case 4:
      return 2*(RADIUS-1) - j;
    case 5:
      return i - j + (RADIUS-1);
    default:
      return i;
  }
}

int posJ(int i, int j) {
  switch(rotation) {
    case 1:
      return (RADIUS-1) - i + j;
    case 2:
      return 2*(RADIUS-1) - i;
    case 3:
      return 2*(RADIUS-1) - j;
    case 4:
      return i - j + (RADIUS-1);
    case 5:
      return i;
    default:
      return j;
  }
}

bool bwCheck(int key) {return (key)%12 == 0 || (key)%12 == 2 || (key)%12 == 4 || (key)%12 == 5 || (key)%12 == 7 || (key)%12 == 9 || (key)%12 == 11;} //1 if white 0 if black

int sectCon(int i) {return buttSector[mode][i];}
int posIJ(int i, int j) {return hexcounterTot(posI(i,j)) + posJ(i,j) + ((posI(i,j) < RADIUS)?0:-(posI(i,j) - (RADIUS-1)));}
int roti(int i) {return posIJ(rowArr[i] - 1,i - hexcounterTot(rowArr[i] - 1) + ((rowArr[i] < (RADIUS+1))?0:(rowArr [i] - RADIUS)));}

int hexcounterTot(int line) {return (line*(2 * RADIUS - 1) - (RADIUS*(RADIUS - 1) + (line - RADIUS + 1)*abs(line - RADIUS)) / 2);}
int hexcounterNum(int line) {return ((2 * RADIUS - 1) - abs(line - RADIUS));}

bool edge(int i) {return board[roti(i)] != pBoard[roti(i)];}
bool rEdge(int i) {return board[roti(i)] && !pBoard[roti(i)];}

/*void smartColor() {
  bool smartColArray[12] = {0};
  int inSmartColor = 1;
  while (board[5][5]) {shiftRead();} //can leave as 5/5 because it doesnt change based on rotation
  while (inSmartColor) {
    shiftRead();
    for (int i = 0; i < RADIUS*2 - 1; i++) {
      for (int j = 0; j < RADIUS*2 - 1; j++) {
        if (abs(i-j) < RADIUS) {
          if (board[i][j] != pBoard[i][j]) {
            int keyPH = 4*(10-i) + 3*(10-j) - 3 + keyShift[mode][buttSector[mode][hexcounterTot(i) + j]] + gKeyShift[mode];
            if (board[i][j]) {
              if(!sentMIDI[mode][buttSector[mode][hexcounterTot(i) + j]][keyPH]){
                usbMIDI.sendNoteOn(keyPH, 127, 1);
              }
              sentMIDI[mode][buttSector[mode][hexcounterTot(i) + j]][keyPH] += 1;
              smartColArray[keyPH%12] = 1;
            }
            else {
              if(sentMIDI[keyPH]){
                usbMIDI.sendNoteOn(keyPH, 0, 1);
              }
              sentMIDI[mode][buttSector[mode][hexcounterTot(i) + j]][keyPH] -= 1;
            }
          }
          pBoard[i][j] = board[i][j];
        }
      }
    }
    for (int i = 0; i < RADIUS*2 - 1; i++) {
      for (int j = 0; j < RADIUS*2 - 1; j++) {
        if (abs(i-j) < RADIUS) {
          if (smartColArray[(4*(10-i) + 3*(10-j) - 3 + keyShift[mode][buttSector[mode][hexcounterTot(i) + j]])%12]) {
            leds[hexcounterTot(i+i%2) + ((i%2)?-j-1:j) - ( (i <= 5)?0:(((i+1)%2)?(i - 5) :(-i + 5)))] = harmColor[mode][buttSector[mode][hexcounterTot(i) + j + ((i < 7)?0:(i - 6))]][1];
          }
          else {
            leds[hexcounterTot(i+i%2) + ((i%2)?-j-1:j) - ( (i <= 5)?0:(((i+1)%2)?(i - 5) :(-i + 5)))] = (harmColor[mode][buttSector[mode][hexcounterTot(i) + j + ((i < 7)?0:(i - 6))]][1] & ~0x030303)/ 4;
          }
        }
      }
    }
    leds[45].r = 127*(sin(millis()/50) + 1);
    FastLED.show();
    //check for key perfection
    int numFit = 0;
    int candidate = 0;
    for (int i = 0; i < 12; i++) {
      bool fit = 1;
      for (int j = 0; j < 12; j++) {
        if (smartColArray[j] && !bwCheck((j + i)%12)) {
          fit = 0;
        }
      }
      if (fit) {numFit++; candidate = i;}
    }
    if (numFit == 1) {
      colShift[mode][editSect] = candidate - 3;
      return;
    }
    else if (!numFit) {
      return;
    }
  }
}*/

