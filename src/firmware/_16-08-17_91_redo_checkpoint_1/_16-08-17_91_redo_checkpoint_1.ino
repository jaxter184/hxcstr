#include <FastLED.h>
#include <SPI.h>

#define RADIUS 6
#define NUM_LEDS 3 * RADIUS * (RADIUS - 1) + 1
#define DATA_PIN 10
#define LATCH 11

int hexcounterTot(int line);  //number of points that precede 'line' linearly
int hexcounterNum(int line);  //number of points in any given line

bool board[RADIUS*2 - 1][RADIUS*2 - 1];
bool pBoard[RADIUS*2 - 1][RADIUS*2 - 1];
int mode = 0;
int editSect = 0;
int buttSector[6][90] = {0};
int sectChannel[6][6] = {0};
byte shiftBank;

int keyShift[6][6] = {33};
int colShift[6][6] = {-3};
int sentMIDI[6][6][128] = {0};
int recMIDI[16][128] = {0};

CRGB leds[NUM_LEDS];

int modeColor[6] = {
  0xFF0000,
  0x888800,
  0x00FF00,
  0x008888,
  0x0000FF,
  0x880088
};

int harmColor[6][6][2] = {
{{0x227722, 0x555555},{0x227722, 0x555555},{0x227722, 0x555555},{0x227722, 0x555555},{0x227722, 0x555555},{0x227722, 0x555555}},
{{0x227722, 0x555555},{0x227722, 0x555555},{0x227722, 0x555555},{0x227722, 0x555555},{0x227722, 0x555555},{0x227722, 0x555555}},
{{0x227722, 0x555555},{0x227722, 0x555555},{0x227722, 0x555555},{0x227722, 0x555555},{0x227722, 0x555555},{0x227722, 0x555555}},
{{0x227722, 0x555555},{0x227722, 0x555555},{0x227722, 0x555555},{0x227722, 0x555555},{0x227722, 0x555555},{0x227722, 0x555555}},
{{0x227722, 0x555555},{0x227722, 0x555555},{0x227722, 0x555555},{0x227722, 0x555555},{0x227722, 0x555555},{0x227722, 0x555555}},
{{0x227722, 0x555555},{0x227722, 0x555555},{0x227722, 0x555555},{0x227722, 0x555555},{0x227722, 0x555555},{0x227722, 0x555555}}
};

int exceptions[][2] = {
  { 4, 4},  { 4, 5},  { 5, 4},  { 5, 6},  { 5,10},  { 6, 5},  { 6, 6},  { 6, 9},  { 7, 8},  { 8, 7},  { 9, 6},  {10, 5}
};

void OnNoteOn(byte channel, byte note, byte velocity)
{
  recMIDI[channel][note] = velocity;
}

void OnNoteOff(byte channel, byte note, byte velocity)
{
  recMIDI[channel][note] = velocity;
}

void setup() {
  SPI.begin ();
  FastLED.addLeds<WS2812B, DATA_PIN, GRB>(leds, NUM_LEDS);
  pinMode (LATCH, OUTPUT);
  digitalWrite (LATCH, HIGH);
  //usbMIDI.sendControlChange()
}

void loop() {
  shiftRead();
  //button registry--------------------------------------------
  int excInd = 0; //exception index. all exceptions should be in numberical order (first by i then by j)
  if (board[10][10]){
    if (board[5][0] && board[0][5]) {editMode();}
          if (board[10][5] &&!pBoard[10][5]){clearMIDI(); mode = 0;}
      else if (board[9][6] && !pBoard[9][6]){clearMIDI(); mode = 1;}
      else if (board[8][7] && !pBoard[8][7]){clearMIDI(); mode = 2;}
      else if (board[7][8] && !pBoard[7][8]){clearMIDI(); mode = 3;}
      else if (board[6][9] && !pBoard[6][9]){clearMIDI(); mode = 4;}
     else if (board[5][10] &&!pBoard[5][10]){clearMIDI(); mode = 5;}
     
      else if (board[9][4] && !pBoard[9][4]){clearMIDI(); editSect = 0;}
      else if (board[8][5] && !pBoard[8][5]){clearMIDI(); editSect = 1;}
      else if (board[7][6] && !pBoard[7][6]){clearMIDI(); editSect = 2;}
      else if (board[6][7] && !pBoard[6][7]){clearMIDI(); editSect = 3;}
      else if (board[5][8] && !pBoard[5][8]){clearMIDI(); editSect = 4;}
      else if (board[4][9] && !pBoard[4][9]){clearMIDI(); editSect = 5;}
     
      else if (board[4][4] && !pBoard[4][4]){clearMIDI(); keyShift[mode][editSect]++;}
      else if (board[6][6] && !pBoard[6][6]){clearMIDI(); keyShift[mode][editSect]--;}
      else if (board[5][4] && !pBoard[5][4]){clearMIDI(); colShift[mode][editSect]++;}
      else if (board[4][5] && !pBoard[4][5]){clearMIDI(); colShift[mode][editSect]--;}
      else if (board[5][6] && !pBoard[5][6]){clearMIDI(); keyShift[mode][editSect]+=(keyShift[mode][editSect] < 48)?12:0;}
      else if (board[6][5] && !pBoard[6][5]){clearMIDI(); keyShift[mode][editSect]-=(keyShift[mode][editSect] > 12)?12:0;}
      else if (board[5][5] && !pBoard[5][5]){clearMIDI(); smartColor();}
      else if (board[10][10] && !pBoard[10][10]){for (int i = 0; i < 16; i++) {for (int j = 0; j < 128; i++) {{recMIDI[i][j] = 0;}}}}
      if (keyShift[mode][editSect] < 0) {keyShift[mode][editSect] = 0;}
      else if (keyShift[mode][editSect] > 60) {keyShift[mode][editSect] = 60;}
  }
  for (int i = 0; i < RADIUS*2 - 1; i++) {
    for (int j = 0; j < RADIUS*2 - 1; j++) {
      if (abs(i-j) < RADIUS) {
        if (board[10][10] && (i == exceptions[excInd][0] && j == exceptions[excInd][1])) {
          excInd++;
        }
        else if (board[i][j] != pBoard[i][j] && !(i == 10 && j == 10)) {
          int keyPH = 4*(10-i) + 3*(10-j) - 3 + keyShift[mode][buttSector[mode][hexcounterTot(i) + j]];
          if (board[i][j]) {
            if(!sentMIDI[mode][buttSector[mode][hexcounterTot(i) + j]][keyPH]){
              usbMIDI.sendNoteOn(keyPH, 127, 1);
            }
            sentMIDI[mode][buttSector[mode][hexcounterTot(i) + j]][keyPH] += 1;
          }
          else {
            if(sentMIDI[mode][buttSector[mode][hexcounterTot(i) + j]][keyPH]){
              usbMIDI.sendNoteOn(keyPH, 0, 1);
            }
            sentMIDI[mode][buttSector[mode][hexcounterTot(i) + j]][keyPH] -= 1;
          }
        }
        pBoard[i][j] = board[i][j];
      }
    }
  }
  //lights----------------------------------------------------
  for (int i = 0; i < RADIUS*2 - 1; i++) {
    for (int j = 0; j < RADIUS*2 - 1; j++) {
      if (abs(i-j) < RADIUS) {
        int keyPH = 4*(10-i) + 3*(10-j) - 3 + keyShift[mode][buttSector[mode][hexcounterTot(i) + j]];
        if (recMIDI[buttSector[mode][hexcounterTot(i) + j]][keyPH]){
          leds[hexcounterTot(i+i%2) + ((i%2)?-j-1:j) - ( (i <= 5)?0:(((i+1)%2)?(i - 5) :(-i + 5)))] = modeColor[mode];
        }
        else if (board[i][j]) {
          leds[hexcounterTot(i+i%2) + ((i%2)?-j-1:j) - ( (i <= 5)?0:(((i+1)%2)?(i - 5) :(-i + 5)))] = harmColor[mode][hexcounterTot(i) + j + ((i < 7)?0:(i - 6))][bwCheck(4*(10-i) + 3*(10-j) 
                                                                                                                                                                + keyShift[mode][buttSector[mode][hexcounterTot(i) + j + ((i < 7)?0:(i - 6))]]
                                                                                                                                                                + colShift[mode][buttSector[mode][hexcounterTot(i) + j + ((i < 7)?0:(i - 6))]])];
        }
        else {
          leds[hexcounterTot(i+i%2) + ((i%2)?-j-1:j) - ( (i <= 5)?0:(((i+1)%2)?(i - 5) :(-i + 5)))] = (harmColor[mode][hexcounterTot(i) + j + ((i < 7)?0:(i - 6))][bwCheck(4*(10-i) + 3*(10-j)
                                                                                                                                                                 + keyShift[mode][buttSector[mode][hexcounterTot(i) + j + ((i < 7)?0:(i - 6))]]
                                                                                                                                                                 + colShift[mode][buttSector[mode][hexcounterTot(i) + j + ((i < 7)?0:(i - 6))]])] & ~0x030303)/ 4;
        }
        leds[90] = (modeColor[mode] & ~0x030303)/ 4;
        if (board[10][10]) {
          leds[85] = (modeColor[0] & ~0x010101)/ 2;
          leds[82] = (modeColor[1] & ~0x010101)/ 2;
          leds[74] = (modeColor[2] & ~0x010101)/ 2;
          leds[63] = (modeColor[3] & ~0x010101)/ 2;
          leds[59] = (modeColor[4] & ~0x010101)/ 2;
          leds[40] = (modeColor[5] & ~0x010101)/ 2;
          
          leds[80] = (modeColor[0] & ~0x030303)/ 4 + (modeColor[mode] & ~0x030303)/ 4;
          leds[76] = (modeColor[1] & ~0x030303)/ 4 + (modeColor[mode] & ~0x030303)/ 4;
          leds[61] = (modeColor[2] & ~0x030303)/ 4 + (modeColor[mode] & ~0x030303)/ 4;
          leds[57] = (modeColor[3] & ~0x030303)/ 4 + (modeColor[mode] & ~0x030303)/ 4;
          leds[38] = (modeColor[4] & ~0x030303)/ 4 + (modeColor[mode] & ~0x030303)/ 4;
          leds[30] = (modeColor[5] & ~0x030303)/ 4 + (modeColor[mode] & ~0x030303)/ 4;
          
          leds[56] = board[6][6]?modeColor[mode]:(modeColor[mode] & ~0x030303)/ 4;
          leds[55] = board[6][5]?modeColor[mode]:(modeColor[mode] & ~0x030303)/ 4;
          leds[46] = board[5][4]?modeColor[mode]:(modeColor[mode] & ~0x030303)/ 4;
          leds[44] = board[5][6]?modeColor[mode]:(modeColor[mode] & ~0x030303)/ 4;
          leds[35] = board[4][5]?modeColor[mode]:(modeColor[mode] & ~0x030303)/ 4;
          leds[34] = board[4][4]?modeColor[mode]:(modeColor[mode] & ~0x030303)/ 4;
          leds[45] = board[5][5]?modeColor[mode]:((~modeColor[mode]) & ~0x030303)/ 4;
          leds[90] = modeColor[mode];
        }
      }
    }
  }
  FastLED.show();
}

void editMode() {
  int inEditMode = 1;
  while (inEditMode){
  }
}

void smartColor() {
  bool smartColArray[12] = {0};
  int inSmartColor = 1;
  while (board[5][5]) {shiftRead();}
  while (inSmartColor) {
    shiftRead();
    for (int i = 0; i < RADIUS*2 - 1; i++) {
      for (int j = 0; j < RADIUS*2 - 1; j++) {
        if (abs(i-j) < RADIUS) {
          if (board[i][j] != pBoard[i][j]) {
            int keyPH = 4*(10-i) + 3*(10-j) - 3 + keyShift[mode][buttSector[mode][hexcounterTot(i) + j]];
            if (board[i][j]) {
              if(!sentMIDI[mode][buttSector[mode][hexcounterTot(i) + j]][keyPH]){
                usbMIDI.sendNoteOn(keyPH, 127, 1);
              }
              sentMIDI[mode][buttSector[mode][hexcounterTot(i) + j]][keyPH] += 1;
              smartColArray[keyPH%12] = 1;
            }
            else {
              if(sentMIDI[keyPH]){
                usbMIDI.sendNoteOn(keyPH, 0, 1);
              }
              sentMIDI[mode][buttSector[mode][hexcounterTot(i) + j]][keyPH] -= 1;
            }
          }
          pBoard[i][j] = board[i][j];
        }
      }
    }
    for (int i = 0; i < RADIUS*2 - 1; i++) {
      for (int j = 0; j < RADIUS*2 - 1; j++) {
        if (abs(i-j) < RADIUS) {
          if (smartColArray[(4*(10-i) + 3*(10-j) - 3 + keyShift[mode][buttSector[mode][hexcounterTot(i) + j]])%12]) {
            leds[hexcounterTot(i+i%2) + ((i%2)?-j-1:j) - ( (i <= 5)?0:(((i+1)%2)?(i - 5) :(-i + 5)))] = harmColor[mode][hexcounterTot(i) + j + ((i < 7)?0:(i - 6))][1];
          }
          else {
            leds[hexcounterTot(i+i%2) + ((i%2)?-j-1:j) - ( (i <= 5)?0:(((i+1)%2)?(i - 5) :(-i + 5)))] = (harmColor[mode][hexcounterTot(i) + j + ((i < 7)?0:(i - 6))][1] & ~0x030303)/ 4;
          }
        }
      }
    }
    leds[45].r = 127*(sin(millis()/50) + 1);
    FastLED.show();
    //check for key perfection
    int numFit = 0;
    bool fit = 0;
    int candidate = 0;
    for (int i = 0; i < 12; i++) {
      bool fit = 1;
      for (int j = 0; j < 12; j++) {
        if (smartColArray[j] && !bwCheck((j + i)%12)) {
          fit = 0;
        }
      }
      if (fit) {numFit++; candidate = i;}
    }
    if (numFit == 1) {
      colShift[mode][editSect] = candidate - 3;
      return;
    }
    else if (!numFit) {
      return;
    }
  }
}

void clearMIDI() {
  for (int i = 0; i < RADIUS*2 - 1; i++) {
    for (int j = 0; j < RADIUS*2 - 1; j++) {
      if (abs(i-j) < RADIUS) {
        int keyPH = 4*(10-i) + 3*(10-j) - 3 + keyShift[mode][buttSector[mode][hexcounterTot(i) + j]];
        sentMIDI[mode][buttSector[mode][hexcounterTot(i) + j]][keyPH] = 0;
        usbMIDI.sendNoteOn(keyPH, 0, 1);
      }
    }
  }
}

void shiftRead() {
  int rowInd = 0;
  digitalWrite (LATCH, LOW);
  digitalWrite (LATCH, HIGH);
  for (int i = 0; i < 3 * RADIUS * (RADIUS - 1) + 1; i++) {
    if (!(i%8)) shiftBank = SPI.transfer(0);
    rowInd += (hexcounterTot(rowInd) == i); //increment rowInd if the index is the beginning of the next row
    board[rowInd - 1][i - hexcounterTot(rowInd - 1) + ((rowInd < 7)?0:(rowInd - 6))] = shiftBank & (1<<(i%8));
  }
}

bool bwCheck(int key) {
  return (key)%12 == 0 || (key)%12 == 2 || (key)%12 == 4 || (key)%12 == 5 || (key)%12 == 7 || (key)%12 == 9 || (key)%12 == 11;
}

int hexcounterTot(int line) {return (line*(2 * RADIUS - 1) - (RADIUS*(RADIUS - 1) + (line - RADIUS + 1)*abs(line - RADIUS)) / 2);}
int hexcounterNum(int line) {return ((2 * RADIUS - 1) - abs(line - RADIUS));}
