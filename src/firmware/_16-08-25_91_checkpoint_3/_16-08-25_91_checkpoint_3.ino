#include <FastLED.h>
#include <SPI.h>

#define RADIUS 6
#define NUM_LEDS 3 * RADIUS * (RADIUS - 1) + 1
#define DATA_PIN 10
#define LATCH 11

int hexcounterTot(int line);  //number of points that precede 'line' linearly
int hexcounterNum(int line);  //number of points in any given line

bool board[RADIUS*2 - 1][RADIUS*2 - 1];
bool pBoard[RADIUS*2 - 1][RADIUS*2 - 1];
int mode = 0;
int editSect = 0;
int rotation = 5;
int buttSector[6][90] = {0};
int sectChannel[6][6] = {0};
byte shiftBank;

int keyShift[6][6] = {{33, 33, 33, 33, 33, 33}};
int colShift[6][6] = {{-3, -3, -3, -3, -3, -3}};
int sentMIDI[6][6][128];
int recMIDI[16][128];

CRGB leds[NUM_LEDS];

int modeColor[6] = {
  0xFF0000,
  0x888800,
  0x00FF00,
  0x008888,
  0x0000FF,
  0x880088
};

int harmColor[6][6][2] = {
{{0x227722, 0x555555},{0x227722, 0x555555},{0x227722, 0x555555},{0x227722, 0x555555},{0x227722, 0x555555},{0x227722, 0x555555}}
};

int exceptions[][2] = {
  { 4, 4},{ 4, 5},{ 4, 9},{ 5, 4},{ 5, 6},{ 5, 8},{ 5,10},{ 6, 5},{ 6, 6},{ 6, 7},{ 6, 9},{ 7, 6},{ 7, 8},{ 8, 5},{ 8, 7},{ 9, 4},{ 9, 6},{10, 5}
};

void OnNoteOn(byte channel, byte note, byte velocity)
{
  recMIDI[channel][note] = velocity;
}

void OnNoteOff(byte channel, byte note, byte velocity)
{
  recMIDI[channel][note] = velocity;
}

void setup() {
  SPI.begin ();
  usbMIDI.setHandleNoteOff(OnNoteOff);
  usbMIDI.setHandleNoteOn(OnNoteOn) ;
  FastLED.addLeds<WS2812B, DATA_PIN, GRB>(leds, NUM_LEDS);
  pinMode (LATCH, OUTPUT);
  digitalWrite (LATCH, HIGH);
  //usbMIDI.sendControlChange()
  clearMIDI();
}

void loop() {
  shiftRead();
  //button registry--------------------------------------------
  int excInd = 0; //exception index. all exceptions should be in numberical order (first by i then by j)
  if (board[posI(0,0)][posJ(0,0)]){
    if (board[posI(5,10)][posJ(5,10)] && board[posI(10,5)][posJ(10,5)]) {editMode();}
           if (rEdge(5,0)){clearMIDI(); mode = 0;}
      else if (rEdge(4,1)){clearMIDI(); mode = 1;}
      else if (rEdge(3,2)){clearMIDI(); mode = 2;}
      else if (rEdge(2,3)){clearMIDI(); mode = 3;}
      else if (rEdge(1,4)){clearMIDI(); mode = 4;}
      else if (rEdge(0,5)){}
     /*
      else if (board[9][4] && !pBoard[9][4]){clearMIDI(); editSect = 0;}
      else if (board[8][5] && !pBoard[8][5]){clearMIDI(); editSect = 1;}
      else if (board[7][6] && !pBoard[7][6]){clearMIDI(); editSect = 2;}
      else if (board[6][7] && !pBoard[6][7]){clearMIDI(); editSect = 3;}
      else if (board[5][8] && !pBoard[5][8]){clearMIDI(); editSect = 4;}
      else if (board[4][9] && !pBoard[4][9]){clearMIDI(); editSect = 5;}
     */
      //else if (rEdge(5,5)){clearMIDI(); smartColor();} //todo: change to recenter
      //else if (board[10][10] && !pBoard[10][10]){for (int i = 0; i < 16; i++) {for (int j = 0; j < 128; j++) {recMIDI[i][j] = 0;}}}
      if (keyShift[mode][editSect] < 0) {keyShift[mode][editSect] = 0;}
      else if (keyShift[mode][editSect] > 60) {keyShift[mode][editSect] = 60;}
  }
  for (int i = 0; i < RADIUS*2 - 1; i++) {
    for (int j = 0; j < RADIUS*2 - 1; j++) {
      if (abs(i-j) < RADIUS) {
        //exceptions
        if (((posI(i,j)+posJ(i,j)) == 0) && rEdge(i,j)){
          clearMIDI();
          mode = j;
        }
        if (board[posI(0,0)][posJ(0,0)] && (posI(i,j) == exceptions[excInd][0] && posJ(i,j) == exceptions[excInd][1])) {
               if (rEdge(4,4)){clearMIDI(); keyShift[mode][editSect]++;}
          else if (rEdge(6,6)){clearMIDI(); keyShift[mode][editSect]--;}
          else if (rEdge(5,4)){clearMIDI(); colShift[mode][editSect]++;}
          else if (rEdge(4,5)){clearMIDI(); colShift[mode][editSect]--;}
          else if (rEdge(5,6)){clearMIDI(); keyShift[mode][editSect]+=(keyShift[mode][editSect] < 48)?12:0;}
          else if (rEdge(6,5)){clearMIDI(); keyShift[mode][editSect]-=(keyShift[mode][editSect] > 12)?12:0;}
          excInd++;
        }
        //midi
        else if (edge(i,j) && (i || j)) {
          int keyPH = 4*(10-i) + 3*(10-j) - 3 + keyShift[mode][buttSector[mode][hexcounterTot(posI(i,j)) + posJ(i,j)]];
          if (board[posI(i,j)][posJ(i,j)]) {
            if(!sentMIDI[mode][buttSector[mode][hexcounterTot(posI(i,j)) + posJ(i,j)]][keyPH]){
              usbMIDI.sendNoteOn(keyPH, 127, 1);
            }
            sentMIDI[mode][buttSector[mode][hexcounterTot(posI(i,j)) + posJ(i,j)]][keyPH]++;
          }
          else {
            if(sentMIDI[mode][buttSector[mode][hexcounterTot(posI(i,j)) + posJ(i,j)]][keyPH]){
              usbMIDI.sendNoteOn(keyPH, 0, 1);
              sentMIDI[mode][buttSector[mode][hexcounterTot(posI(i,j)) + posJ(i,j)]][keyPH]--;
            }
          } 
        }
        pBoard[posI(i,j)][posJ(i,j)] = board[posI(i,j)][posJ(i,j)];
      }
    }
  }
  //lights----------------------------------------------------
  for (int i = 0; i < RADIUS*2 - 1; i++) {
    for (int j = 0; j < RADIUS*2 - 1; j++) {
      if (abs(i-j) < RADIUS) {
        int keyPH = 4*(10-i) + 3*(10-j) - 3 + keyShift[mode][buttSector[mode][hexcounterTot(posI(i,j)) + posJ(i,j)]];
        usbMIDI.read();
        if (recMIDI[sectChannel[mode][buttSector[mode][hexcounterTot(i) + j + ((i < 7)?0:(i - 6))]]][keyPH]){
          leds[ledCon(i,j)] = modeColor[mode];
        }
        else if (board[posI(i,j)][posJ(i,j)]) {
          leds[ledCon(i,j)] = harmColor[mode][buttSector[mode][hexcounterTot(i) + j + ((i < 7)?0:(i - 6))]][bwCheck(4*(10-i) + 3*(10-j) 
                                                                                                                                          + keyShift[mode][buttSector[mode][hexcounterTot(i) + j + ((i < 7)?0:(i - 6))]]
                                                                                                                                          + colShift[mode][buttSector[mode][hexcounterTot(i) + j + ((i < 7)?0:(i - 6))]])];
        }
        else {
          leds[ledCon(i,j)] = (harmColor[mode][buttSector[mode][hexcounterTot(i) + j + ((i < 7)?0:(i - 6))]][bwCheck(4*(10-i) + 3*(10-j)
                                                                                                                                          + keyShift[mode][buttSector[mode][hexcounterTot(i) + j + ((i < 7)?0:(i - 6))]]
                                                                                                                                          + colShift[mode][buttSector[mode][hexcounterTot(i) + j + ((i < 7)?0:(i - 6))]])] & ~0x030303)/ 4;
        }
        leds[ledCon(0,0)] = (modeColor[mode] & ~0x030303)/ 4;
        if (board[posI(0,0)][posJ(0,0)]) {
          leds[ledCon(5,0)] = (modeColor[0] & ~0x010101)/ 2;
          leds[ledCon(4,1)] = (modeColor[1] & ~0x010101)/ 2;
          leds[ledCon(3,2)] = (modeColor[2] & ~0x010101)/ 2;
          leds[ledCon(2,3)] = (modeColor[3] & ~0x010101)/ 2;
          leds[ledCon(1,4)] = (modeColor[4] & ~0x010101)/ 2;
          leds[ledCon(0,5)] = (modeColor[5] & ~0x010101)/ 2;
          /*
          leds[ledCon(posI(9,4), posJ(9,4))] = (modeColor[0] & ~0x070707)/ 8 + (modeColor[mode] & ~0x030303)/ 4;
          leds[ledCon(posI(8,5), posJ(8,5))] = (modeColor[1] & ~0x070707)/ 8 + (modeColor[mode] & ~0x030303)/ 4;
          leds[ledCon(posI(7,6), posJ(7,6))] = (modeColor[2] & ~0x070707)/ 8 + (modeColor[mode] & ~0x030303)/ 4;
          leds[ledCon(posI(6,7), posJ(6,7))] = (modeColor[3] & ~0x070707)/ 8 + (modeColor[mode] & ~0x030303)/ 4;
          leds[ledCon(posI(5,8), posJ(5,8))] = (modeColor[4] & ~0x070707)/ 8 + (modeColor[mode] & ~0x030303)/ 4;
          leds[ledCon(posI(4,9), posJ(4,9))] = (modeColor[5] & ~0x070707)/ 8 + (modeColor[mode] & ~0x030303)/ 4;
          */
          leds[ledCon(6,6)] = board[posI(6,6)][posJ(6,6)]?modeColor[mode]:((modeColor[mode] & ~0x030303)/ 4);
          leds[ledCon(6,5)] = board[posI(6,5)][posJ(6,5)]?modeColor[mode]:((modeColor[mode] & ~0x030303)/ 4);
          leds[ledCon(5,4)] = board[posI(5,4)][posJ(5,4)]?modeColor[mode]:((modeColor[mode] & ~0x030303)/ 4);
          leds[ledCon(5,6)] = board[posI(5,6)][posJ(5,6)]?modeColor[mode]:((modeColor[mode] & ~0x030303)/ 4);
          leds[ledCon(4,5)] = board[posI(4,5)][posJ(4,5)]?modeColor[mode]:((modeColor[mode] & ~0x030303)/ 4);
          leds[ledCon(4,4)] = board[posI(4,4)][posJ(4,4)]?modeColor[mode]:((modeColor[mode] & ~0x030303)/ 4);
          leds[ledCon(5,5)] = board[posI(5,5)][posJ(5,5)]?modeColor[mode]:((~modeColor[mode]) & ~0x030303)/ 4;
          leds[ledCon(0,0)] = modeColor[mode];
        }
      }
    }
  }
  FastLED.show();
}

void editMode() {
  int inEditMode = 1;
  for (int i = 0; i < 91; i++) {
    leds[i] = CRGB::Black;
  }
  FastLED.show();
  while (board[posI(0,0)][posJ(0,0)]){
    shiftRead();
  }
  while (inEditMode){
    shiftRead();
    for (int i = 0; i < RADIUS*2 - 1; i++) {
      for (int j = 0; j < RADIUS*2 - 1; j++) {
        if (abs(i-j) < RADIUS) {
          /*if (board[posI(0,0)][posJ(0,0)] && (posI(i,j) == exceptions[excInd][0] && posJ(i,j) == exceptions[excInd][1])) {
            excInd++;
          }
          else if (board[i][j] != pBoard[i][j] && !(!i && !j)) {
            int keyPH = 4*(10-posI(i,j)) + 3*(10-posJ(i,j)) - 3 + keyShift[mode][buttSector[mode][hexcounterTot(posI(i,j)) + posJ(i,j)]];
            if (board[posI(i,j)][posJ(i,j)]) {
              if(!sentMIDI[mode][buttSector[mode][hexcounterTot(posI(i,j)) + posJ(i,j)]][keyPH]){
                usbMIDI.sendNoteOn(keyPH, 127, 1);
              }
              sentMIDI[mode][buttSector[mode][hexcounterTot(posI(i,j)) + posJ(i,j)]][keyPH]++;
            }
            else {
              if(sentMIDI[mode][buttSector[mode][hexcounterTot(posI(i,j)) + posJ(i,j)]][keyPH]){
                usbMIDI.sendNoteOn(keyPH, 0, 1);
                sentMIDI[mode][buttSector[mode][hexcounterTot(posI(i,j)) + posJ(i,j)]][keyPH]--;
              }
            } 
          }*/
          pBoard[posI(i,j)][posJ(i,j)] = board[posI(i,j)][posJ(i,j)];
        }
      }
    }
    FastLED.show();
    if (board[posI(0,0)][posJ(0,0)] && board[posI(5,10)][posJ(5,10)] && board[posI(10,5)][posJ(10,5)]) {inEditMode = 0;}
  }
  while (board[posI(0,0)][posJ(0,0)]){
    shiftRead();
  }
}

void smartColor() {
  bool smartColArray[12] = {0};
  int inSmartColor = 1;
  while (board[5][5]) {shiftRead();} //can leave as 5/5 because it doesnt change based on rotation
  while (inSmartColor) {
    shiftRead();
    for (int i = 0; i < RADIUS*2 - 1; i++) {
      for (int j = 0; j < RADIUS*2 - 1; j++) {
        if (abs(i-j) < RADIUS) {
          if (board[i][j] != pBoard[i][j]) {
            int keyPH = 4*(10-i) + 3*(10-j) - 3 + keyShift[mode][buttSector[mode][hexcounterTot(i) + j]];
            if (board[i][j]) {
              if(!sentMIDI[mode][buttSector[mode][hexcounterTot(i) + j]][keyPH]){
                usbMIDI.sendNoteOn(keyPH, 127, 1);
              }
              sentMIDI[mode][buttSector[mode][hexcounterTot(i) + j]][keyPH] += 1;
              smartColArray[keyPH%12] = 1;
            }
            else {
              if(sentMIDI[keyPH]){
                usbMIDI.sendNoteOn(keyPH, 0, 1);
              }
              sentMIDI[mode][buttSector[mode][hexcounterTot(i) + j]][keyPH] -= 1;
            }
          }
          pBoard[i][j] = board[i][j];
        }
      }
    }
    for (int i = 0; i < RADIUS*2 - 1; i++) {
      for (int j = 0; j < RADIUS*2 - 1; j++) {
        if (abs(i-j) < RADIUS) {
          if (smartColArray[(4*(10-i) + 3*(10-j) - 3 + keyShift[mode][buttSector[mode][hexcounterTot(i) + j]])%12]) {
            leds[hexcounterTot(i+i%2) + ((i%2)?-j-1:j) - ( (i <= 5)?0:(((i+1)%2)?(i - 5) :(-i + 5)))] = harmColor[mode][buttSector[mode][hexcounterTot(i) + j + ((i < 7)?0:(i - 6))]][1];
          }
          else {
            leds[hexcounterTot(i+i%2) + ((i%2)?-j-1:j) - ( (i <= 5)?0:(((i+1)%2)?(i - 5) :(-i + 5)))] = (harmColor[mode][buttSector[mode][hexcounterTot(i) + j + ((i < 7)?0:(i - 6))]][1] & ~0x030303)/ 4;
          }
        }
      }
    }
    leds[45].r = 127*(sin(millis()/50) + 1);
    FastLED.show();
    //check for key perfection
    int numFit = 0;
    int candidate = 0;
    for (int i = 0; i < 12; i++) {
      bool fit = 1;
      for (int j = 0; j < 12; j++) {
        if (smartColArray[j] && !bwCheck((j + i)%12)) {
          fit = 0;
        }
      }
      if (fit) {numFit++; candidate = i;}
    }
    if (numFit == 1) {
      colShift[mode][editSect] = candidate - 3;
      return;
    }
    else if (!numFit) {
      return;
    }
  }
}

void clearMIDI() {
  for (int i = 0; i < RADIUS*2 - 1; i++) {
    for (int j = 0; j < RADIUS*2 - 1; j++) {
      if (abs(i-j) < RADIUS) {
        int keyPH = 4*(10-i) + 3*(10-j) - 3 + keyShift[mode][buttSector[mode][hexcounterTot(i) + j]];
        sentMIDI[mode][buttSector[mode][hexcounterTot(i) + j]][keyPH] = 0;
        usbMIDI.sendNoteOn(keyPH, 0, 1);
      }
    }
  }
}

void shiftRead() {
  int rowInd = 0;
  digitalWrite (LATCH, LOW);
  digitalWrite (LATCH, HIGH);
  for (int i = 0; i < 3 * RADIUS * (RADIUS - 1) + 1; i++) {
    if (!(i%8)) shiftBank = SPI.transfer(0);
    rowInd += (hexcounterTot(rowInd) == i); //increment rowInd if the index is the beginning of the next row
    board[rowInd - 1][i - hexcounterTot(rowInd - 1) + ((rowInd < 7)?0:(rowInd - 6))] = shiftBank & (1<<(i%8));
  }
}

int posI(int i, int j) {
  switch(rotation) {
    case 1:
      return j;
    case 2:
      return 5 - i + j;
    case 3:
      return 10 - i;
    case 4:
      return 10 - j;
    case 5:
      return i - j + 5;
    default:
      return i;
  }
}

int posJ(int i, int j) {
  switch(rotation) {
    case 1:
      return 5 - i + j;
    case 2:
      return 10 - i;
    case 3:
      return 10 - j;
    case 4:
      return i - j + 5;
    case 5:
      return i;
    default:
      return j;
  }
}

bool bwCheck(int key) {return (key)%12 == 0 || (key)%12 == 2 || (key)%12 == 4 || (key)%12 == 5 || (key)%12 == 7 || (key)%12 == 9 || (key)%12 == 11;}

int ledCon(int i, int j) {return hexcounterTot(posI(i,j)+posI(i,j)%2) + ((posI(i,j)%2)?-posJ(i,j)-1:posJ(i,j)) - ( (posI(i,j) <= 5)?0:(((posI(i,j)+1)%2)?(posI(i,j) - 5) :(-posI(i,j) + 5)));}

int hexcounterTot(int line) {return (line*(2 * RADIUS - 1) - (RADIUS*(RADIUS - 1) + (line - RADIUS + 1)*abs(line - RADIUS)) / 2);}
int hexcounterNum(int line) {return ((2 * RADIUS - 1) - abs(line - RADIUS));}

bool edge(int i, int j) {return board[posI(i,j)][posJ(i,j)] != pBoard[posI(i,j)][posJ(i,j)];}
bool rEdge(int i, int j) {return board[posI(i,j)][posJ(i,j)] && !pBoard[posI(i,j)][posJ(i,j)];}

