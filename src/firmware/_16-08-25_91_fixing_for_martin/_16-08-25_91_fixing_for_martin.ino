#include <FastLED.h>
#include <SPI.h>

#define RADIUS 6
#define TOTAL 3 * RADIUS * (RADIUS - 1) + 1
#define DATA_PIN 10
#define LATCH 11

/*int hexcounterTot(int line);  //number of points that precede 'line' linearly
int hexcounterNum(int line);  //number of points in any given line
void initConverters();*/

int debug = 50;

bool board[TOTAL];
bool pBoard[TOTAL];
int mode = 0;
int editSect = 0;
int rotation = 1;
int buttSector[6][TOTAL] = {0};
int converter[3][TOTAL];
int rowArr[91];
int ledArr[91];
//int sectChannel[6][6] = {0};
byte shiftBank;

int keyShift[6][6] = {{33, 33, 33, 33, 33, 33},{33, 33, 33, 33, 33, 33}};
int colShift[6][6] = {{-3, -3, -3, -3, -3, -3},{-3, -3, -3, -3, -3, -3}};
int layout[6][6] = {{0},{1},{2},{0},{0},{0}};
int gKeyShift[6] = {0,0,0,0,0,0};
int gColShift[6] = {0,0,0,0,0,0};
int sentMIDI[6][6][128];
int recMIDI[16][128];

CRGB leds[TOTAL];

int modeColor[6] = {
  0xFF0000,
  0x888800,
  0x00FF00,
  0x008888,
  0x0000FF,
  0x880088
};

int harmColor[6][6][2] = {
{{0x227722, 0x555555},{0x227722, 0x555555},{0x227722, 0x555555},{0x227722, 0x555555},{0x227722, 0x555555},{0x227722, 0x555555}},
{{0x227722, 0x555555},{0x227722, 0x555555},{0x227722, 0x555555},{0x227722, 0x555555},{0x227722, 0x555555},{0x227722, 0x555555}}
};

void OnNoteOn(byte channel, byte note, byte velocity)
{
  //recMIDI[channel][note] = velocity;
  recMIDI[1][note] = velocity;
}

void OnNoteOff(byte channel, byte note, byte velocity)
{
  //recMIDI[channel][note] = velocity;
  recMIDI[1][note] = 0;
}

void setup() {
  Serial.begin(9600);
  SPI.begin ();
  usbMIDI.setHandleNoteOff(OnNoteOff);
  usbMIDI.setHandleNoteOn(OnNoteOn) ;
  FastLED.addLeds<WS2812B, DATA_PIN, GRB>(leds, TOTAL);
  FastLED.setBrightness(128);
  pinMode (LATCH, OUTPUT);
  digitalWrite (LATCH, HIGH);
  initConverters();
  initRowArr();
  //usbMIDI.sendControlChange()
  clearMIDI();
}

void loop() {
  shiftRead();
  //button registry--------------------------------------------
  if (board[roti(0)]) {
    if (rEdge(34)){clearMIDI(); gKeyShift[mode]++;}
    if (rEdge(56)){clearMIDI(); gKeyShift[mode]--;}
    if (rEdge(35)){gColShift[mode]++;}
    if (rEdge(44)){gColShift[mode]--;}
    if (rEdge(46)){clearMIDI(); gKeyShift[mode]+=12;}
    if (rEdge(45)){clearMIDI(); gKeyShift[mode] = 0; gColShift[mode] = 0;}
    if (rEdge(55)){clearMIDI(); gKeyShift[mode]-=12;}
    for (int eachSector = 0; eachSector < 6; eachSector++) {
      if (keyShift[mode][eachSector] < 0) {keyShift[mode][eachSector] = 0;}
      else if (keyShift[mode][eachSector] > 60) {keyShift[mode][eachSector] = 60;}
    }
  }
  for (int i = 0; i < TOTAL; i++) {
    //exceptions
    if (board[roti(0)] && (i==90) && rEdge(90)) {
      editMode();
    }
    else if (board[roti(0)] && ((rowArr[i] - 1 + i - hexcounterTot(rowArr[i] - 1) + ((rowArr[i] < 7)?0:(rowArr[i] - 6))) == 5) && rEdge(i)){ //horizontal row
      clearMIDI();
      mode = 6 - rowArr[i];
    }
    else if (board[roti(0)] && (i == 34 || i == 35 || i == 44 || i == 45 || i == 46 || i == 55 || i == 56)) {} //i and j dont need to be rotated because its the middle
    else if (i == 0) {}
    //midi
    else if (edge(i) && buttSector[mode][i] != 6) {
      int keyPH = converter[layout[mode][buttSector[mode][i]]][i] + keyShift[mode][buttSector[mode][i]] + gKeyShift[mode];
      if (board[roti(i)]) {
        if(!sentMIDI[mode][buttSector[mode][i]][keyPH]){
          usbMIDI.sendNoteOn(keyPH, 127, buttSector[mode][i] + 1);
        }
        sentMIDI[mode][buttSector[mode][i]][keyPH]++;
      }
      else {
        if(sentMIDI[mode][buttSector[mode][i]][keyPH]){
          usbMIDI.sendNoteOn(keyPH, 0, buttSector[mode][i] + 1);
          sentMIDI[mode][buttSector[mode][i]][keyPH]--;
        } 
      }
    }
    pBoard[roti(i)] = board[roti(i)];
  }
  //lights----------------------------------------------------
  for (int i = 0; i < TOTAL; i++) {
    int keyPH = converter[layout[mode][buttSector[mode][i]]][i] + keyShift[mode][buttSector[mode][i]] + gKeyShift[mode];
    usbMIDI.read();
    //if (recMIDI[sectChannel[mode][buttSector[mode][sectCon(i,j)][keyPH]){
    if (recMIDI[buttSector[mode][i] + 1][keyPH]){
      leds[ledArr[roti(i)]] = modeColor[mode];
    }
    else if (buttSector[mode][i] == 6) {
      leds[ledArr[roti(i)]] = CRGB::Black;
    }
    else if (board[roti(i)]) {
      leds[ledArr[roti(i)]] = harmColor[mode][buttSector[mode][i]][bwCheck(converter[layout[mode][buttSector[mode][i]]][i] + keyShift[mode][buttSector[mode][i]] + colShift[mode][buttSector[mode][i]] + gKeyShift[mode] + gColShift[mode])];
    }
    else {
      leds[ledArr[roti(i)]] = (harmColor[mode][buttSector[mode][i]][bwCheck(converter[layout[mode][buttSector[mode][i]]][i] + keyShift[mode][buttSector[mode][i]] + colShift[mode][buttSector[mode][i]] + gKeyShift[mode] + gColShift[mode])] & ~0x030303)/ 4;
    }
    /*if (i == debug) {
      leds[ledArr[roti(i)]].setHue(i);
      Serial.print(i);
      Serial.print(" - ");
      Serial.print(rowArr[i] - 1);
      //Serial.print(posI(rowArr[i] - 1,i - hexcounterTot(rowArr[i] - 1) + ((rowArr[i] < 7)?0:-(rowArr[i] - 7))));
      Serial.print(",");
      //Serial.println(posJ(rowArr[i] - 1,i - hexcounterTot(rowArr[i] - 1) + ((rowArr[i] < 7)?0:-(rowArr[i] - 7))));
      Serial.print(i - hexcounterTot(rowArr[i] - 1) + ((rowArr[i] < 7)?0:(rowArr [i] - 6)));
      Serial.print(" : ");
      Serial.println(roti(i));
    }
    else {
      leds[ledArr[roti(i)]] = CRGB::Black;
    }*/
    if (board[roti(0)]) {
      if ((rowArr[i] - 1 + i - hexcounterTot(rowArr[i] - 1) + ((rowArr[i] < 7)?0:(rowArr[i] - 6))) == 5){
        leds[ledArr[roti(i)]] = (modeColor[6 - rowArr[i]] & ~0x010101)/ 2;
      }
      leds[ledArr[56]] = board[56]?modeColor[mode]:((modeColor[mode] & ~0x030303)/ 4);
      leds[ledArr[46]] = board[46]?modeColor[mode]:((modeColor[mode] & ~0x030303)/ 4);
      leds[ledArr[35]] = board[35]?modeColor[mode]:((modeColor[mode] & ~0x030303)/ 4);
      leds[ledArr[55]] = board[55]?modeColor[mode]:((modeColor[mode] & ~0x030303)/ 4);
      leds[ledArr[44]] = board[44]?modeColor[mode]:((modeColor[mode] & ~0x030303)/ 4);
      leds[ledArr[34]] = board[34]?modeColor[mode]:((modeColor[mode] & ~0x030303)/ 4);
      leds[ledArr[45]] = board[45]?modeColor[mode]:((~modeColor[mode]) & ~0x070707)/ 8;
      leds[ledArr[roti(0)]] = modeColor[mode];
      leds[ledArr[roti(90)]] = board[90]?modeColor[mode]:((modeColor[mode] & ~0x030303)/ 4);
    }
    else {
      leds[ledArr[roti(0)]] = (modeColor[mode] & ~0x030303)/ 4;
    }
  }
  FastLED.show();
  /*debug++;
  delay(100);
  if (debug > 91) {
    debug = 0;
  }*/
}

void initConverters() {
  int index = 0;
  for (int i = 0; i < RADIUS*2 - 1; i++) {
    for (int j = 0; j < RADIUS*2 - 1; j++) {
      if (abs(i-j) < RADIUS) {
        converter[0][index] = 4*(10-i) + 3*(10-j) - 3;
        index++;
      }
    }
  }
  index = 0;
  for (int i = 0; i < RADIUS*2 - 1; i++) {
    for (int j = 0; j < RADIUS*2 - 1; j++) {
      if (abs(i-j) < RADIUS) {
        converter[1][index] = 5*(10-i) - 7*(10-j) + 45;
        index++;
      }
    }
  }
  index = 0;
  for (int i = 0; i < RADIUS*2 - 1; i++) {
    for (int j = 0; j < RADIUS*2 - 1; j++) {
      if (abs(i-j) < RADIUS) {
        ledArr[index] = hexcounterTot(i + i%2) + ((i%2)?-j-1:j) - ((i <= 5)?0:(((i+1)%2)?(i - 5):(-i + 5)));
        index++;
      }
    }
  }
}

void initRowArr() {
 int eachUnit = 0;
  for (int rows = 1; rows < (2 * RADIUS); rows++) {
    for (int units = 0; units < hexcounterNum(rows); units++) {
      rowArr[eachUnit] = rows;
      eachUnit++;
    }
  }
}

void clearMIDI() {
  for (int i = 0; i < RADIUS*2 - 1; i++) {
    for (int j = 0; j < RADIUS*2 - 1; j++) {
      if (abs(i-j) < RADIUS) {
        int keyPH = 4*(10-i) + 3*(10-j) - 3 + keyShift[mode][buttSector[mode][hexcounterTot(i) + j]] + gKeyShift[mode];
        sentMIDI[mode][buttSector[mode][hexcounterTot(i) + j]][keyPH] = 0;
        usbMIDI.sendNoteOn(keyPH, 0, 1);
      }
    }
  }
}

void clearInCache() {
  for (int i = 0; i < 6; i++) {
    for (int j = 0; j < 127; j++) {
      recMIDI[i][j] = 0;
    }
  }
}

void shiftRead() {
  //int rowInd = 0;
  digitalWrite (LATCH, LOW);
  digitalWrite (LATCH, HIGH);
  for (int i = 0; i < TOTAL; i++) {
    if (!(i%8)) {shiftBank = SPI.transfer(0);}
    //rowInd += (hexcounterTot(rowInd) == i); //increment rowInd if the index is the beginning of the next row
    //board[rowInd - 1][i - hexcounterTot(rowInd - 1) + ((rowInd < 7)?0:(rowInd - 6))] = shiftBank & (1<<(i%8));
    board[i] = shiftBank & (1<<(i%8));
  }
}

int posI(int i, int j) {
  switch(rotation) {
    case 1:
      return j;
    case 2:
      return 5 - i + j;
    case 3:
      return 10 - i;
    case 4:
      return 10 - j;
    case 5:
      return i - j + 5;
    default:
      return i;
  }
}

int posJ(int i, int j) {
  switch(rotation) {
    case 1:
      return 5 - i + j;
    case 2:
      return 10 - i;
    case 3:
      return 10 - j;
    case 4:
      return i - j + 5;
    case 5:
      return i;
    default:
      return j;
  }
}

bool bwCheck(int key) {return (key)%12 == 0 || (key)%12 == 2 || (key)%12 == 4 || (key)%12 == 5 || (key)%12 == 7 || (key)%12 == 9 || (key)%12 == 11;} //1 if white 0 if black

//int ledCon(int i, int j) {return hexcounterTot(posI(i,j)+posI(i,j)%2) + ((posI(i,j)%2)?-posJ(i,j)-1:posJ(i,j)) - ( (posI(i,j) <= 5)?0:(((posI(i,j)+1)%2)?(posI(i,j) - 5) :(-posI(i,j) + 5)));}
//int sectCon(int i, int j) {return buttSector[mode][hexcounterTot(i) + j + ((i < 7)?0:(i - 6))];}
int sectCon(int i) {return buttSector[mode][i];}
int posIJ(int i, int j) {return hexcounterTot(posI(i,j)) + posJ(i,j) + ((posI(i,j) < 6)?0:-(posI(i,j) - 5));}
//int roti(int i) {return posIJ(rowArr[i] - 1,i - hexcounterTot(rowArr[i] - 1) + ((rowArr[i] < 7)?0:-(rowArr[i] - 7)));}
int roti(int i) {return posIJ(rowArr[i] - 1,i - hexcounterTot(rowArr[i] - 1) + ((rowArr[i] < 7)?0:(rowArr [i] - 6)));}

int hexcounterTot(int line) {return (line*(2 * RADIUS - 1) - (RADIUS*(RADIUS - 1) + (line - RADIUS + 1)*abs(line - RADIUS)) / 2);}
int hexcounterNum(int line) {return ((2 * RADIUS - 1) - abs(line - RADIUS));}

bool edge(int i) {return board[roti(i)] != pBoard[roti(i)];}
bool rEdge(int i) {return board[roti(i)] && !pBoard[roti(i)];}



void editMode() {
  clearMIDI();
  clearInCache();
  int inEditMode = 1;
  for (int i = 0; i < 91; i++) {
    leds[i] = CRGB::Black;
  }
  FastLED.show();
  pBoard[roti(90)] = 1;
  while (inEditMode){
    shiftRead();
    if (board[roti(0)]) { //i and j dont need to be rotated because its the middle
      if (rEdge(34)){clearMIDI(); keyShift[mode][editSect]++;}
      if (rEdge(56)){clearMIDI(); keyShift[mode][editSect]--;}
      if (rEdge(35)){colShift[mode][editSect]++;}
      if (rEdge(44)){colShift[mode][editSect]--;}
      if (rEdge(55)){clearMIDI(); keyShift[mode][editSect]+=12;}
      //if (rEdge(45)){clearMIDI(); smartColor();}
      if (rEdge(47)){clearMIDI(); keyShift[mode][editSect]-=12;}
      if (rEdge(85)){rotation = (rotation + 1) % 6;}
      if (rEdge(50)){rotation = (rotation + 5) % 6;} //same as -1 except allows for 0 - 1 to equal 5
      if (rEdge(90)) {inEditMode = 0;}
      if (keyShift[mode][editSect] < 0) {keyShift[mode][editSect] = 0;}
      else if (keyShift[mode][editSect] > 60) {keyShift[mode][editSect] = 60;}
    }
    for (int i = 0; i < TOTAL; i++) {
      if (i == 0){}
      else if (board[roti(0)]){
        if (((rowArr[roti(i)] - 1 + i - hexcounterTot(rowArr[i] - 1) + ((rowArr[i] < 7)?0:(rowArr[i] - 6))) == 5) && rEdge(i)){
          //editSect = j;
          editSect = 6 - rowArr[i];
        }
        else if (i == 34 || i == 35 || i == 44 || i == 45 || i == 46 || i == 55 || i == 56) { //i and j dont need to be rotated because its the middle
        }
        else if (rEdge(90)){}
        else if (rEdge(i)) {
          editSect = 6;
        }
      }
      else if (edge(i)) {
        if (sectCon(i) == editSect) {
          int keyPH = converter[layout[mode][buttSector[mode][i]]][i] + keyShift[mode][buttSector[mode][i]] + gKeyShift[mode];
          if (board[roti(i)]) {
            if(!sentMIDI[mode][sectCon(i)][keyPH]){
              usbMIDI.sendNoteOn(keyPH, 127, sectCon(i) + 1);
            }
            sentMIDI[mode][buttSector[mode][i]][keyPH]++;
          }
          else {
              usbMIDI.sendNoteOn(keyPH, 0, sectCon(i) + 1);
            if(sentMIDI[mode][buttSector[mode][i]][keyPH]){
              sentMIDI[mode][buttSector[mode][i]][keyPH]--;
            } 
          }
        }
        else {
          buttSector[mode][i] = editSect;
        }
      }
      pBoard[roti(i)] = board[roti(i)];
    }
    //edit lights----------------------------------------------------
    for (int i = 0; i < TOTAL; i++) {
      if (buttSector[mode][i] == 6) {
        leds[ledArr[roti(i)]] = CRGB::Black;
      }
      else {
          //int keyPH = 4*(10-i) + 3*(10-j) - 3 + keyShift[mode][buttSector[mode][hexcounterTot(posI(i,j)) + posJ(i,j)]] + gKeyShift[mode];
          int keyPH = converter[layout[mode][buttSector[mode][i]]][i] + keyShift[mode][buttSector[mode][i]] + gKeyShift[mode];
          leds[ledArr[roti(i)]] = blend(0x000000, blend(modeColor[sectCon(i)], modeColor[mode], 127), 30 + 40 * bwCheck(keyPH + colShift[mode][buttSector[mode][i]] + gColShift[mode]));
          //leds[ledArr[roti(i)]] = harmColor[mode][buttSector[mode][i]][bwCheck(converter[layout[mode][buttSector[mode][i]]][i] + keyShift[mode][buttSector[mode][i]] + colShift[mode][buttSector[mode][i]] + gKeyShift[mode] + gColShift[mode])];
          leds[ledArr[roti(0)]] = (modeColor[mode] & ~0x030303)/ 4;
      }
      if (board[roti(0)]) {
        if ((rowArr[roti(i)] - 1 + i - hexcounterTot(rowArr[i] - 1) + ((rowArr[i] < 7)?0:(rowArr[i] - 6))) == 5){
          leds[ledArr[roti(i)]] = blend(0x000000, blend(modeColor[6 - rowArr[i]], modeColor[mode], 127), 75);
        }
      }
    }
    if (board[roti(0)]) {
      leds[ledArr[roti(56)]] = board[roti(56)]?modeColor[mode]:(((modeColor[mode] & ~0x030303)/ 4) + 0x111111);
      leds[ledArr[roti(46)]] = board[roti(46)]?modeColor[mode]:(((modeColor[mode] & ~0x030303)/ 4) + 0x111111);
      leds[ledArr[roti(35)]] = board[roti(35)]?modeColor[mode]:(((modeColor[mode] & ~0x030303)/ 4) + 0x111111);
      leds[ledArr[roti(55)]] = board[roti(35)]?modeColor[mode]:(((modeColor[mode] & ~0x030303)/ 4) + 0x111111);
      leds[ledArr[roti(44)]] = board[roti(44)]?modeColor[mode]:(((modeColor[mode] & ~0x030303)/ 4) + 0x111111);
      leds[ledArr[roti(34)]] = board[roti(34)]?modeColor[mode]:(((modeColor[mode] & ~0x030303)/ 4) + 0x111111);
      leds[ledArr[roti(45)]] = board[roti(45)]?modeColor[mode]:((~modeColor[mode]) & ~0x070707)/ 8;
      leds[ledArr[roti(85)]] = 0x505050;
      leds[ledArr[roti(50)]] = 0x505050;
      leds[ledArr[roti(90)]] = board[roti(90)]?modeColor[mode]:((modeColor[mode] & ~0x030303)/ 4);
      leds[ledArr[roti(0)]] = blend(0x000000, blend(modeColor[editSect], modeColor[mode], 127), 200);
    }
    else {  
      //leds[ledArr[roti(i)]] = blend(0x000000, blend(modeColor[editSect], modeColor[mode], 127), 75);
    }
    if (editSect == 6) {
      //leds[ledArr[roti(i)]] = CRGB::Black;
    }
    FastLED.show();
  }
  clearMIDI();
}

/*void smartColor() {
  bool smartColArray[12] = {0};
  int inSmartColor = 1;
  while (board[5][5]) {shiftRead();} //can leave as 5/5 because it doesnt change based on rotation
  while (inSmartColor) {
    shiftRead();
    for (int i = 0; i < RADIUS*2 - 1; i++) {
      for (int j = 0; j < RADIUS*2 - 1; j++) {
        if (abs(i-j) < RADIUS) {
          if (board[i][j] != pBoard[i][j]) {
            int keyPH = 4*(10-i) + 3*(10-j) - 3 + keyShift[mode][buttSector[mode][hexcounterTot(i) + j]] + gKeyShift[mode];
            if (board[i][j]) {
              if(!sentMIDI[mode][buttSector[mode][hexcounterTot(i) + j]][keyPH]){
                usbMIDI.sendNoteOn(keyPH, 127, 1);
              }
              sentMIDI[mode][buttSector[mode][hexcounterTot(i) + j]][keyPH] += 1;
              smartColArray[keyPH%12] = 1;
            }
            else {
              if(sentMIDI[keyPH]){
                usbMIDI.sendNoteOn(keyPH, 0, 1);
              }
              sentMIDI[mode][buttSector[mode][hexcounterTot(i) + j]][keyPH] -= 1;
            }
          }
          pBoard[i][j] = board[i][j];
        }
      }
    }
    for (int i = 0; i < RADIUS*2 - 1; i++) {
      for (int j = 0; j < RADIUS*2 - 1; j++) {
        if (abs(i-j) < RADIUS) {
          if (smartColArray[(4*(10-i) + 3*(10-j) - 3 + keyShift[mode][buttSector[mode][hexcounterTot(i) + j]])%12]) {
            leds[hexcounterTot(i+i%2) + ((i%2)?-j-1:j) - ( (i <= 5)?0:(((i+1)%2)?(i - 5) :(-i + 5)))] = harmColor[mode][buttSector[mode][hexcounterTot(i) + j + ((i < 7)?0:(i - 6))]][1];
          }
          else {
            leds[hexcounterTot(i+i%2) + ((i%2)?-j-1:j) - ( (i <= 5)?0:(((i+1)%2)?(i - 5) :(-i + 5)))] = (harmColor[mode][buttSector[mode][hexcounterTot(i) + j + ((i < 7)?0:(i - 6))]][1] & ~0x030303)/ 4;
          }
        }
      }
    }
    leds[45].r = 127*(sin(millis()/50) + 1);
    FastLED.show();
    //check for key perfection
    int numFit = 0;
    int candidate = 0;
    for (int i = 0; i < 12; i++) {
      bool fit = 1;
      for (int j = 0; j < 12; j++) {
        if (smartColArray[j] && !bwCheck((j + i)%12)) {
          fit = 0;
        }
      }
      if (fit) {numFit++; candidate = i;}
    }
    if (numFit == 1) {
      colShift[mode][editSect] = candidate - 3;
      return;
    }
    else if (!numFit) {
      return;
    }
  }
}*/


