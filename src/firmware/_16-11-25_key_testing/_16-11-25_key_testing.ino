#include <SPI.h>

#define RADIUS 6
#define TOTAL 3 * RADIUS * (RADIUS - 1) + 1
#define LATCH 11

bool board[TOTAL];
bool pBoard[TOTAL];
byte shiftBank;
void setup() {
  Serial.begin(9600);
  SPI.begin();
  pinMode (LATCH, OUTPUT);
  digitalWrite (LATCH, HIGH);
}

void loop() {
  shiftRead();
  Serial.print(rEdge(43));
  Serial.print(",");
  Serial.println(fEdge(43));
  pBoard[43] = board[43];
  delay(50);
}

void shiftRead() {
  //int rowInd = 0;
  digitalWrite (LATCH, LOW);
  digitalWrite (LATCH, HIGH);
  for (int i = 0; i < TOTAL; i++) {
    if (!(i % 8)) {
      shiftBank = SPI.transfer(0);
    }
    board[i] = shiftBank & (1 << (i % 8));
  }
}
bool edge(int i) {
  return board[i] != pBoard[i];
}
bool rEdge(int i) {
  return board[i] && !pBoard[i];
}
bool fEdge(int i) {
  return !board[i] && pBoard[i];
}

