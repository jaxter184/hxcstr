#include <FastLED.h>
#include <SPI.h>

#define RADIUS 6
#define NUM_LEDS 3 * RADIUS * (RADIUS - 1) + 1
#define DATA_PIN 10
#define LATCH 11

int hexcounterTot(int line);  //number of points that precede 'line' linearly
int hexcounterNum(int line);  //number of points in any given line

bool board[RADIUS*2 - 1][RADIUS*2 - 1];
bool pBoard[RADIUS*2 - 1][RADIUS*2 - 1];
byte shiftBank;
int keyShift = 36;
CRGB leds[NUM_LEDS];

void setup() {
  SPI.begin ();
  Serial.begin(11520);
  FastLED.addLeds<WS2812B, DATA_PIN, GRB>(leds, NUM_LEDS);
  pinMode (LATCH, OUTPUT);
  digitalWrite (LATCH, HIGH);
}

void loop() {
  shiftRead();
  for (int i = 0; i < RADIUS*2 - 1; i++) {
    for (int j = 0; j < RADIUS*2 - 1; j++) {
      if (abs(i-j) < RADIUS) {
        if (board[i][j] != pBoard[i][j]) {
          if (board[i][j]) {
            /*Serial.print(i);
            Serial.print(", ");
            Serial.print(j);
            Serial.print(", ");
            Serial.println(hexcounterTot(i+i%2) + ((i%2)?-j-1:j) - ( (i <= 5)?0:(((i+1)%2)?(i - 5) :(-i + 5))));*/
            leds[hexcounterTot(i+i%2) + ((i%2)?-j-1:j) - ( (i <= 5)?0:(((i+1)%2)?(i - 5) :(-i + 5)))] = CRGB::Green;
            usbMIDI.sendNoteOn(4*(10-i) + 3*(10-j) + keyShift, 127, 1);
          }
          else {
            leds[hexcounterTot(i+i%2) + ((i%2)?-j-1:j) - ( (i <= 5)?0:(((i+1)%2)?(i - 5) :(-i + 5)))] = CRGB::Black;
            usbMIDI.sendNoteOn(4*(10-i) + 3*(10-j) + keyShift, 0, 1);
          }
        }
        pBoard[i][j] = board[i][j];
      }
    }
  }
  FastLED.show();
}

/*void shiftRead() {
  int index, rowInd = 0;
  digitalWrite (LATCH, LOW);
  digitalWrite (LATCH, HIGH);
  for (int i = 0; i < 12; i++) {
    shiftBank = SPI.transfer(0);
    for (int j = 0; j < 8; j++) {
      if (index < 3 * RADIUS * (RADIUS - 1) + 1) {
        rowInd += (hexcounterTot(rowInd) == index); //increment rowInd if the index is the beginning of the next row
        board[rowInd - 1][index - hexcounterTot(rowInd - 1) + ((rowInd < 7)?0:(rowInd - 6))] = shiftBank & (1<<j);
        index++;
      }
    }
  }
}*/
void shiftRead() {
  int rowInd = 0;
  digitalWrite (LATCH, LOW);
  digitalWrite (LATCH, HIGH);
  for (int i = 0; i < 3 * RADIUS * (RADIUS - 1) + 1; i++) {
    if (!(i%8)){
      shiftBank = SPI.transfer(0);
    }
    rowInd += (hexcounterTot(rowInd) == i); //increment rowInd if the index is the beginning of the next row
    board[rowInd - 1][i - hexcounterTot(rowInd - 1) + ((rowInd < 7)?0:(rowInd - 6))] = shiftBank & (1<<(i%8));
  }
}

int hexcounterTot(int line) {return (line*(2 * RADIUS - 1) - (RADIUS*(RADIUS - 1) + (line - RADIUS + 1)*abs(line - RADIUS)) / 2);}
int hexcounterNum(int line) {return ((2 * RADIUS - 1) - abs(line - RADIUS));}
