## File overview

This repo is less of an actual repository and more of an archive of the project files for the HXCSTR, a hexagonal isomorphic MIDI controller. The project was made before I knew what git was, but I've reorganized many of the files to better fit my current file heirarchy scheme.

### src

The most egregious offender of disorganization is the firmware source code. A clear indicator of that is the fact that the first project is called `redo`, as I had to remake the firmware after accidentally deleting (or corrupting? I don't remember) the original firmware. There are also many files suffixed with `checkpoint`, as I did not use version control. There are some files, such as `rotation` and `wicki-hayden` that branch off other projects, but don't specify which. They also add features that are not present in more recent files. In the end, I don't think it's worth sorting through all the files to figure out which ones I can keep and which ones to toss out until I rewrite the code. I also don't think the files have any way of specifying the target architecture, so for future reference, they were written for the Teensy 3.1.

### mch

* `shell.svg` - Laser cut acrylic outer shell
* `hexcap.fcstd` - Injection mold keycaps
* `keycap.blend` - 3D printed keycap

### img

* `boardoutline.svg` - Outline for PCB
* `KCRef.svg` - Measurement reference for punching in numbers while making the PCB. Not sure what KC stands for. Maybe 'key cap'?

### pcb

The names are pretty self explanatory, I think. I never ended up using the 'tactile' board. I also don't think the `lib` folder is actually linked because the directories have been tumbled around so much.

## TODO

Rewrite code in rust
Switch buffers instead of reading over. One is read and the other is write
Acceleration is determined by previous position
Velocity is determined by acceleration and previous velocity (only one buffer required for both read and write)
Position is determined by velocity and previous position

state machine instrument
Note layout depends on previous note output
using circle of fifths
